#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H

#include <GLFW/glfw3.h>
#include <vector>
#include <functional>
#include "matrix.h"

class IO {
private:
	GLFWwindow* window;
public:
	const GLuint UP = 0, RIGHT = 1, DOWN = 2, LEFT = 3, SPACE = 4;
	GLuint keyStates[5] = { GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE };
	GLuint keys[5] = { GLFW_KEY_UP, GLFW_KEY_RIGHT, GLFW_KEY_DOWN, GLFW_KEY_LEFT, GLFW_KEY_SPACE };
	std::vector<std::function<void()>> holdEvents[5] = { std::vector<std::function<void()>>(), std::vector<std::function<void()>>(), std::vector<std::function<void()>>(), std::vector<std::function<void()>>(), std::vector<std::function<void()>>() };
	std::vector<std::function<void()>> pressEvents[5] = { std::vector<std::function<void()>>(), std::vector<std::function<void()>>(), std::vector<std::function<void()>>(), std::vector<std::function<void()>>(), std::vector<std::function<void()>>() };

	IO(GLFWwindow& glfw);
	Matrix update();
};

#endif