#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "inputHandler.h"
#include "shader.h"
#include "shape.h"
#include "matrix.h"
#include "vector.h"
#include "camera.h"
#include "renderer.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

const float ROT = 1;
const float SCALE = .05;
const float TRANS = .01;
bool firstPerson = false;
std::vector<Shader> shaders = std::vector<Shader>();
Matrix* matrixBeingModified;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) { glViewport(0, 0, width, height); }
void errorCallback(int error, const char* description) { fprintf(stderr, "GLFW Error: %s\n", description); }

void processFunction(void *function(void *data), void* data) { function(data); }
void transPosX() { Matrix trans; trans.translate(TRANS, 0, 0); *matrixBeingModified = *matrixBeingModified * trans; }
void transNegX() { Matrix trans; trans.translate(-TRANS, 0, 0); *matrixBeingModified = *matrixBeingModified * trans; }
void transPosZ() { Matrix trans; trans.translate(0, 0, TRANS); *matrixBeingModified = *matrixBeingModified * trans; }
void transNegZ() { Matrix trans; trans.translate(0, 0, -TRANS); *matrixBeingModified = *matrixBeingModified * trans; }
void changeView() { std::cout << "Toggled space!" << std::endl; }

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Chris' Maze - Program 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

	IO handler = IO(*window);

	Shape duck("../models/dino.obj", "Dinosaur");
	shaders.push_back(Shader("../vert.glsl", "../frag.glsl", duck));
	//shaders[0].getShape()->worldPos.scale(0.0001, 0.0001, 0.0001);

    // setup camera
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

    Camera camera;
    camera.projection = projection;
    camera.eye = Vector(0, 0, 3);
    camera.origin = Vector(0, 0, 0);
    camera.up = Vector(0, 1, 0);

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // The Renderer class does the heavy lifting required by the drawing components in each iteration of the main loop.
    Renderer renderer;

    // set the light position
    Vector lightPos(3.75f, 3.75f, 4.0f);
	
	matrixBeingModified = &shaders[0].getShape()->worldPos;

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
		*matrixBeingModified = handler.update();

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		for (int i = 0; i < shaders.size(); i++) {
			renderer.render(camera, shaders[i], lightPos);
		}

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
	
	glDeleteVertexArrays(1, &shaders[0].vao);
	glDeleteBuffers(1, &shaders[0].vbo);
	glDeleteBuffers(1, &shaders[0].ebo);

    glfwTerminate();
    return 0;
}
