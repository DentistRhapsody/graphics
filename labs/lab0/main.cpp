#include <iostream>
#include <string>
using namespace std;

class Vector3 {  // This class has been directly copied, no original content is in here.
public:
	float x;
	float y;
	float z;

	// Default Constructor
	Vector3() {
		x = 0;
		y = 0;
		z = 0;
	}

	// Constructor
	Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
		// nothing to do here as we've already initialized x, y, and z above
		std::cout << "in Vector3 constructor" << std::endl;
	}

	// Destructor - called when an object goes out of scope or is destroyed
	~Vector3() {
		// this is where you would release resources such as memory or file descriptors
		// in this case we don't need to do anything
		std::cout << "in Vector3 destructor" << std::endl;
	}

	Vector3 operator+(Vector3& v) {
		return Vector3(v.x + x, v.y + y, v.z + z);
	}

	Vector3 operator*(float& s) {
		return Vector3(x * s, y * s, z * s);
	}
};

std::ostream& operator<<(std::ostream& stream, Vector3& v) {
	stream << "(" << v.x << "," << v.y << "," << v.z << ")";
	return stream;
}


Vector3 add(Vector3& v, Vector3& v2) {
	return Vector3(v.x + v2.x, v.y + v2.y, v.z + v2.z);
}

int main(int argc, char** argv) {
	// Part 4.a
	for (int i = 0; i < argc; i++) {
		cout << argv[i] << endl;
	}
	
	// Part 4.b
	cout << "Please enter your name: ";
	string name;
	cin >> name;
	cout << "hello " << name << endl;

	// Part 5
	Vector3 a(1, 2, 3);
	Vector3 b(4, 5, 6);

	Vector3 c = add(a, b);
	cout << "Summed vector: (" << c.x << "," << c.y << "," << c.z << ")" << endl;

	// Part 6
	Vector3 d = a + b;
	cout << "Summed using the + operator: " << d << endl;

	// Part 7
	Vector3 seven(0, 0, 0);
	Vector3* pt_seven = &seven;
	pt_seven->y = 5;
	cout << seven;

	// Part 8
	Vector3 vectors[ 10 ];
	for (int i = 0; i < 10; i++) {
		vectors[i].y = 5;
	}
	for (int i = 0; i < 10; i++) {
		cout << vectors[i];
	}

	string exit;
	cin >> exit;
}