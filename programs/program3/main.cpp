#define _USE_MATH_DEFINES

#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtx/normal.hpp>
#include <math.h>
#include "bitmap_image.hpp"

const int WIDTH = 640;
const int HEIGHT = 480;
const rgb_t BACKGROUND = make_colour(200, 200, 255);

// Lighting.
const glm::vec3 lightPos = glm::vec3(0, 0, -5.0);
const glm::vec3 lightColor = glm::vec3(1.0, 1.0, 1.0);
const float ambientStrength = 0.2;
const float specularStrength = 0.5;

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Ray {
	int id;
	glm::vec3 origin;
	glm::vec3 direction;

	Ray(const glm::vec3& origin = glm::vec3(0, 0, 0),
		const glm::vec3& direction = glm::normalize(glm::vec3(1, 1, 1)))
		: origin(origin), direction(glm::normalize(direction)) {
		static int id_seed = 0;
		id = ++id_seed;
	}
};

struct Triangle {
	glm::vec3 corners[3];
	glm::vec3 color;
	glm::vec3 normal;
	bool isReflective = false;

	Triangle(const glm::vec3& corner1, glm::vec3& corner2, glm::vec3& corner3, glm::vec3& normal, glm::vec3& color) : corners{ corner1, corner2, corner3 }, normal(normal), color(color) { isReflective = false; };
	Triangle(const glm::vec3& corner1, glm::vec3& corner2, glm::vec3& corner3, glm::vec3& normal) : corners{ corner1, corner2, corner3 }, normal(normal) { isReflective = false; };
	Triangle(const glm::vec3& corner1, glm::vec3& corner2, glm::vec3& corner3) : corners{ corner1, corner2, corner3 } {};
};

struct Shape {
	int id;
	std::vector<Triangle> mesh;

	glm::vec3 color;

	Shape(const glm::vec3& color=glm::vec3(0.1, 0.1, 0.1)) : color(color) {
		static int id_seed = 0;
		id = ++id_seed;
	}

	std::pair<float, Triangle> getCollision(Ray r) {
		for (Triangle poly : mesh) {
			glm::vec3 n = poly.normal;
			float nDir = glm::dot(n, r.direction);
			if (nDir > 0) {
				float d = glm::dot(n, poly.corners[0]);
				float t = (glm::dot(n, r.origin) + d) / nDir;
				if (t >= 0) {
					glm::vec3 p = r.origin + t * r.direction;
					glm::vec3 perp = glm::cross(poly.corners[1] - poly.corners[0], p - poly.corners[0]);
					if (glm::dot(n, perp) >= 0) {
						perp = glm::cross(poly.corners[2] - poly.corners[1], p - poly.corners[1]);
						if (glm::dot(n, perp) >= 0) {
							perp = glm::cross(poly.corners[0] - poly.corners[2], p - poly.corners[2]);
							if (glm::dot(n, perp) > 0) {
								return std::make_pair(t, poly);
							}
						}
					}
				}
			}
		}
		return std::make_pair(-1, mesh[0]);
	}
};

struct Box : Shape {
	glm::vec3 center;
	float scale;

	Box(const glm::vec3& center=glm::vec3(0,0,0), float scale=1, const glm::vec3& color=glm::vec3(0,0,0))
		: center(center), scale(scale), Shape(color) {
		buildMesh();
	}

	void buildMesh() {
		std::vector<float> vertices = {
			-0.5, -0.5, -0.5, //0.0, 0.0, 0.0,
			-0.5, -0.5, 0.5, //0.0, 0.0, 1.0,
			-0.5, 0.5, -0.5, //0.0, 1.0, 0.0,
			-0.5, 0.5, 0.5, //0.0, 1.0, 1.0,
			0.5, -0.5, -0.5, //1.0, 0.0, 0.0,
			0.5, -0.5, 0.5, //1.0, 0.0, 1.0,
			0.5, 0.5, -0.5, //1.0, 1.0, 0.0,
			0.5, 0.5, 0.5, //1.0, 1.0, 1.0
		};
		std::vector<int> faces = {
			0, 1, 2, 1, 3, 2,
			0, 6, 4, 0, 2, 6,
			4, 7, 5, 4, 6, 7,
			3, 5, 7, 3, 1, 5,
			2, 3, 7, 2, 7, 6,
			0, 4, 5, 0, 5, 1
		};
		for (int i = 0; i < faces.size(); i+=3) {
			glm::vec3 corners[3];
			for (int f = 0; f < 3; f++) {
				int index = faces[i + f];
				corners[f] = scale * glm::vec3(vertices[index * 3 + 0], vertices[index * 3 + 1], vertices[index * 3 + 2]) + center;
			}
			glm::vec3 normal = glm::triangleNormal(corners[0], corners[1], corners[2]);
			mesh.push_back(Triangle(corners[0], corners[1], corners[2], normal, color));
		}
	}
};

struct Sphere : Shape {
    glm::vec3 center;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0), float radius=0, const glm::vec3& color=glm::vec3(0,0,0))
		: center(center), radius(radius), Shape(color) {
		buildMesh();
	}

	/* Implementation of sphere-intercept ray tracing. Returns -1 if there is no intersection. */
	float tDistance(const Ray& r) {  
		glm::vec3 toCenter = center - r.origin;
		float v = glm::dot(toCenter, r.direction);
		float disc = radius * radius - (glm::dot(toCenter, toCenter) - v * v);
		if (disc > 0) {
			float d = sqrt(disc);
			return v - d;
		}
		return -1;
	};

	float x(float radius, float phi, float theta) { return radius * cos(theta) * sin(phi); }
	float y(float radius, float phi, float theta) { return -radius * cos(phi); }
	float z(float radius, float phi, float theta) { return -radius * sin(theta) * sin(phi); }

	void buildMesh() {
		int layers = 30;
		double stepPhi = M_PI / layers;
		double stepTheta = 2 * M_PI / layers;

		for (int j = 0; j <= layers; ++j) {
			for (int i = 0; i <= layers; ++i) {
				//std::cout << i << "," << j << std::endl;
				double phi = i * stepPhi;
				double phiNext = (i + 1) * stepPhi;
				double theta = -M_PI + j * stepTheta;
				double thetaNext = -M_PI + (j + 1) * stepTheta;

				glm::vec3 c0(x(radius, phi, theta), y(radius, phi, theta), z(radius, phi, theta));
				glm::vec3 c1(x(radius, phiNext, theta), y(radius, phiNext, theta), z(radius, phiNext, theta));
				glm::vec3 c2(x(radius, phi, thetaNext), y(radius, phi, thetaNext), z(radius, phi, thetaNext));
				glm::vec3 c3(x(radius, phiNext, thetaNext), y(radius, phiNext, thetaNext), z(radius, phiNext, thetaNext));
				c0 += center;
				c1 += center;
				c2 += center;
				c3 += center;
				glm::vec3 normal = glm::triangleNormal(c0, c1, c2);
				mesh.push_back(Triangle(c0, c1, c2, normal, color));
				mesh.push_back(Triangle(c2, c1, c3, normal, color));
			}
		}
	}
};

glm::vec3& collides(const Ray r, const Triangle poly) {
	glm::vec3 pvec = glm::cross(r.direction, poly.corners[2] - poly.corners[0]);
	float determinant = glm::dot(poly.corners[1] - poly.corners[0], pvec);
	if (determinant <= 0.000001)
		return glm::vec3(0, 0, 0);
	glm::vec3 tvec = r.origin - poly.corners[0];
	float u = glm::dot(tvec, pvec) * (1 / determinant);
	if (u < 0 || u > 1)
		return glm::vec3(0, 0, 0);
	glm::vec3 qvec = glm::cross(tvec, poly.corners[1] - poly.corners[0]);
	float v = glm::dot(r.direction, qvec) * (1 / determinant);
	if (v < 0 || v + u > 1)
		return glm::vec3(0, 0, 0);
	return r.origin + (glm::dot(poly.corners[2] - poly.corners[0], qvec) * (1 / determinant)) * r.direction;
}

void render(bitmap_image& image, /*std::vector<Sphere>& world,*/ const float cameraOffset=-1, std::vector<Triangle> polys={}) {
	Viewport vp(glm::vec2(-5, -5), glm::vec2(5, 5));
	float l = vp.min.x, r = vp.max.x, b = vp.min.y, t = vp.max.y;
	for (int x = 0; x < WIDTH; x++) {
		std::cout << "Beginning row #" << x << " of " << WIDTH << " (" << (x * 100.0 / WIDTH) << "% done)" << std::endl;
		for (int y = 0; y < HEIGHT; y++) {
			// Build the ray for this specific pixel.
			float ux = l + (r - l) * (x + 0.5) / WIDTH;
			float vy = b + (t - b) * (y + 0.5) / HEIGHT;
			Ray r = Ray(glm::vec3(0, 0, -cameraOffset), glm::vec3(ux, -vy, cameraOffset));
			rgb_t pixel = BACKGROUND;

			
			float closest = INT_MAX;
			int polyIndex = -1;  // This is the index of the closest polygon in the soup.
			glm::vec3 point;
			for (int p = 0; p < polys.size(); p++) {
				glm::vec3 tp = collides(r, polys[p]);
				if (glm::length(tp) > 0.00001 && glm::length(tp) < closest) {
					closest = glm::length(tp);
					polyIndex = p;
					point = tp;
				}
			}
			if (polyIndex != -1) { // If a polygon was hit by this ray, light it.

				/*if (polys[polyIndex].isReflective) {
					glm::vec3 reflectDir = glm::reflect(-r.direction, -polys[polyIndex].normal);
					Ray ray = Ray(r.origin + r.direction * closest, reflectDir);
					
					float refClosest = INT_MAX;
					int refPolyIndex = -1;  // This is the index of the closest polygon in the soup.
					glm::vec3 refPoint;
					for (int p = 0; p < polys.size(); p++) {
						glm::vec3 tp = collides(ray, polys[p]);
						if (glm::length(tp) > 0.00001 && glm::length(tp) < refClosest) {
							refClosest = glm::length(tp);
							refPolyIndex = p;
							refPoint = tp;
						}
					}
					if (refPolyIndex >= 0) {
						r = ray;
						closest = INT_MAX;
						point = refPoint;
						polyIndex = refPolyIndex;
						glm::vec3 lightDir = glm::normalize(lightPos - point);
						float angleToLight = glm::dot(polys[polyIndex].normal, lightDir);
						if (angleToLight < 0)
							angleToLight = 0.0;
						glm::vec3 diffuse = lightColor * angleToLight;
						//color = (ambientStrength + color) * polys[polyIndex].color;
						//glm::vec3 color = glm::vec3(closest / 10, closest / 10, closest / 10);

						glm::vec3 reflectDir = glm::reflect(-lightDir, -polys[polyIndex].normal);
						float placeholder = glm::dot(r.direction, reflectDir);
						if (placeholder < 0)
							placeholder = 0.0;
						placeholder = pow(placeholder, 32);
						glm::vec3 specular = specularStrength * placeholder * lightColor;
						glm::vec3 color = (ambientStrength + diffuse + specular) * (0.9f * polys[polyIndex].color);
						pixel = make_colour(color.x * 255, color.y * 255, color.z * 255);
					}
				} 
				else {*/
					glm::vec3 lightDir = glm::normalize(lightPos - point);
					float angleToLight = glm::dot(polys[polyIndex].normal, lightDir);
					if (angleToLight < 0)
						angleToLight = 0.0;
					glm::vec3 diffuse = lightColor * angleToLight;
					//color = (ambientStrength + color) * polys[polyIndex].color;
					//glm::vec3 color = glm::vec3(closest / 10, closest / 10, closest / 10);

					glm::vec3 reflectDir = glm::reflect(-lightDir, polys[polyIndex].normal);
					float placeholder = glm::dot(r.direction, reflectDir);
					if (placeholder < 0)
						placeholder = 0.0;
					placeholder = pow(placeholder, 32);
					glm::vec3 specular = specularStrength * placeholder * lightColor;
					glm::vec3 color = (ambientStrength + diffuse + specular) * polys[polyIndex].color;
					pixel = make_colour(color.x * 255, color.y * 255, color.z * 255);
			}
			image.set_pixel(x, y, pixel);
		}
	}
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(WIDTH, HEIGHT);
	image.set_all_channels(0, 0, 0);
	// image_drawer g(image);

	// build world
	std::vector<Sphere> sphereWorld = {
		//Sphere(glm::vec3(0, 0, 1), 1, glm::vec3(1,1,0)),
		//Sphere(glm::vec3(1, 1, 4), 2, glm::vec3(0,1,1)),
		//Sphere(glm::vec3(5, 2, 6), 3, glm::vec3(1,0,1)),
		//Sphere(glm::vec3(-2, -2, 20), 15, glm::vec3(0.2, 0.2, 0.2)),
		Sphere(glm::vec3(0.5, 0.5, 0.75), 0.5, glm::vec3(0.7, 0, 0.2))
    };

	std::vector<Box> boxWorld = {
		Box(glm::vec3(0, -6, 4), 2, glm::vec3(0.8, 0.1, 0.1)),
		Box(glm::vec3(-4, 4, 3), 2, glm::vec3(0.1, 0.8, 0.8))
	};

	std::vector<Triangle> allPolys;
	for (int i = 0; i < boxWorld.size(); i++)
		allPolys.insert(allPolys.end(), boxWorld[i].mesh.begin(), boxWorld[i].mesh.end());
	for (int i = 0; i < sphereWorld.size(); i++)
		allPolys.insert(allPolys.end(), sphereWorld[i].mesh.begin(), sphereWorld[i].mesh.end());

	// Add a mirror to demonstrate reflectivity.
	Triangle mirror = Triangle(glm::vec3(-4, -4, 4), glm::vec3(4, 4, 4), glm::vec3(4, -4, 1));
	mirror.normal = glm::triangleNormal(mirror.corners[0], mirror.corners[1], mirror.corners[2]);
	mirror.color = glm::vec3(0.4, 0.6, 0.8);
	mirror.isReflective = true;
	allPolys.push_back(mirror);

    // render the world
    //render(image, world);
    //image.save_image("../orthographic.bmp");
	render(image, 5, allPolys);
	image.save_image("../output.bmp");
    std::cout << "Success" << std::endl;
}


