#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <GLFW/glfw3.h>
#include "shape.h"

class Shader {
private:
	GLuint _id;
	Shape shape;
	
	GLuint createShader(const std::string& fileName, GLenum shaderType);
	GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader);

public:
	GLuint vbo, vao, ebo;
	Shader(std::string vertShader, std::string fragShader, Shape& s);
	GLuint id();
	void use();
	Shape* getShape();
};

#endif