#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_
#define	_USE_MATH_DEFINES
#include <cmath>
#include <math.h>
#include <string>

#include <cstdlib>
#include <vector>
#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

template <typename T, typename N, typename C>
void add_vertex(T& coords, const N& x, const N& y, const N& z,
        const C& r, const C& g, const C& b, bool with_noise=false) {
    // adding color noise makes it easier to see before shading is implemented
    float noise = 1-with_noise*(rand()%150)/100.;
    coords.push_back(x);
    coords.push_back(y);
    coords.push_back(z);
    coords.push_back(r*noise);
    coords.push_back(g*noise);
    coords.push_back(b*noise);
}

template <typename T, typename N, typename C>
void add_vertex(T& coords, const N& x, const N& y, const N& z,
	const C& nx, const C& ny, const C& nz, const C& r,
	const C& g, const C& b, bool with_noise = false) {
	// adding color noise makes it easier to see before shading is implemented
	float noise = 1 - with_noise*(rand() % 150) / 100.;
	coords.push_back(x);
	coords.push_back(y);
	coords.push_back(z);
	coords.push_back(nx);
	coords.push_back(ny);
	coords.push_back(nz);
	coords.push_back(r*noise);
	coords.push_back(g*noise);
	coords.push_back(b*noise);
}

class Shape {
public:
	std::vector<float> coords;
	Matrix model;
	GLuint VAO;
};

class Triangle: public Shape {
public:
	Triangle() {
		coords = {
			-0.5f, 0.5f, 0.0f, 0.0f, 0.7f,
			0.5f, 0.5f, 0.0f, 0.0f, 0.7f,
			0.5f, -0.5f, 0.0f, 0.0f, 0.7f
		};
	}
};

class Square: public Shape {
public:
	Square() {
		coords = {
			-0.5f, -0.5f, 0.0f, 0.7f, 0.0f,
			-0.5f, 0.5f, 0.0f, 0.7f, 0.0f,
			0.5f, 0.5f, 0.0f, 0.7f, 0.0f,
			0.5f, 0.5f, 0.0f, 0.7f, 0.0f,
			-0.5f, -0.5f, 0.0f, 0.7f, 0.0f,
			0.5f, -0.5f, 0.0f, 0.7f, 0.0f
		};
	}
};

class Circle: public Shape {
public:
	Circle(int precision=20) {
		for (int i = 0; i < precision; i++) {
			double theta = i * (2 * M_PI / precision);
			double theta2 = ((i + 1) % precision) * (2 * M_PI / precision);
			double x1 = cos(theta) / 2;
			double y1 = sin(theta) / 2;
			double x2 = cos(theta2) / 2;
			double y2 = sin(theta2) / 2;
			coords.push_back(x1);
			coords.push_back(y1);
			coords.push_back(0.7f);
			coords.push_back(0.0f);
			coords.push_back(0.0f);
			coords.push_back(0.0f);
			coords.push_back(0.0f);
			coords.push_back(0.7f);
			coords.push_back(0.0f);
			coords.push_back(0.0f);
			coords.push_back(x2);
			coords.push_back(y2);
			coords.push_back(0.7f);
			coords.push_back(0.0f);
			coords.push_back(0.0f);
		}
	}
};

class DiscoCube {
public:
    std::vector<float> coords;
    DiscoCube() : coords{
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  -1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  1.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  1.0f, 1.0f, 0.0f
    } {}

};

class Cylinder {
public:
    std::vector<float> coords;
    Cylinder(unsigned int n, float r, float g, float b) {
        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            // vertex i
            double theta_i = i*step_size;
            double vi_x = radius*cos(theta_i);
            double vi_y = radius*sin(theta_i);

            // vertex i+1
            double theta_ip1 = ((i+1)%n)*step_size;
            double vip1_x = radius*cos(theta_ip1);
            double vip1_y = radius*sin(theta_ip1);

            // add triangle viL, viH, vip1L
			float v1x = 0, v1y = 0, v1z = h + h;
			float v2x = vip1_x - vi_x, v2y = vip1_y - vi_y, v2z = 0;
			float nx = v1y*v2z - v1z*v2y, ny = v1x*v2z - v1z*v2x, nz = v1x*v2y - v1y*v2x;
			float dist = -sqrt(nx*nx + ny*ny + nz*nz);
			nx /= dist, ny /= dist, nz /= dist;
            add_vertex(coords, vi_x, vi_y, -h, nx, -ny, nz, r, g, b);
            add_vertex(coords, vi_x, vi_y, h, nx, -ny, nz, r, g, b);
            add_vertex(coords, vip1_x, vip1_y, -h, nx, -ny, nz, r, g, b);

            // add triangle vip1L, viH, vip1H
			v1x = vi_x - vip1_x, v1y = vi_y - vip1_y, v1z = h + h;
			v2x = 0, v2y = 0, v2z = h + h;
			dist = sqrt(nx*nx + ny*ny + nz*nz);
			nx /= dist, ny /= dist, nz /= dist;
            add_vertex(coords, vip1_x, vip1_y, -h, nx, -ny, nz, r, g, b);
            add_vertex(coords, vi_x, vi_y, h, nx, -ny, nz, r, g, b);
            add_vertex(coords, vip1_x, vip1_y, h, nx, -ny, nz, r, g, b);

            // add high triangle vi, vip1, 0
			v1x = vi_x - vip1_x, v1y = vi_y - vip1_y, v1z = 0;
			v2x = c_x - vip1_x, v2y = c_y - vip1_y, v2z = 0;
			nx = v1y*v2z - v1z*v2y, ny = v1x*v2z - v1z*v2x, nz = v1x*v2y - v1y*v2x;
			dist = -sqrt(nx*nx + ny*ny + nz*nz);
			nx /= dist, ny /= dist, nz /= dist;
            add_vertex(coords, vip1_x, vip1_y, h, nx, ny, nz, r, g, b);
            add_vertex(coords, vi_x, vi_y, h, nx, ny, nz, r, g, b);
            add_vertex(coords, c_x, c_y, h, nx, ny, nz, r, g, b);

            // // add low triangle vi, vip1, 0
			nx *= -1, ny *= -1, nz *= -1;
            add_vertex(coords, vip1_x, vip1_y, -h, nx, ny, nz, r, g, b);
            add_vertex(coords, c_x, c_y, -h, nx, ny, nz, r, g, b);
            add_vertex(coords, vi_x, vi_y, -h, nx, ny, nz, r, g, b);
        }
    }
};


class Cone {
public:
    std::vector<float> coords;
    Cone(unsigned int n, float r, float g, float b) {

        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            // vertex i
            double theta_i = i*step_size;
            double vi_x = radius*cos(theta_i);
            double vi_y = radius*sin(theta_i);

            // vertex i+1
            double theta_ip1 = ((i+1)%n)*step_size;
            double vip1_x = radius*cos(theta_ip1);
            double vip1_y = radius*sin(theta_ip1);

            // add triangle viL, viH, vip1L
            add_vertex(coords, vi_x, vi_y, -h, r, g, b);
            add_vertex(coords, c_x, c_y, h, r, g, b);
            add_vertex(coords, vip1_x, vip1_y, -h, r, g, b);

            // // add low triangle vi, vip1, 0
            add_vertex(coords, vip1_x, vip1_y, -h, r, g, b);
            add_vertex(coords, c_x, c_y, -h, r, g, b);
            add_vertex(coords, vi_x, vi_y, -h, r, g, b);
        }
    }
};

class Sphere {
    double x(float r, float phi, float theta){
        return r*cos(theta)*sin(phi);
    }

    double y(float r, float phi, float theta){
        return r*sin(theta)*sin(phi);
    }

    double z(float r, float phi, float theta){
        return r*cos(phi);
    }

public:
    std::vector<float> coords;
    Sphere(unsigned int n, float radius, float r, float g, float b) {
        int n_steps = (n%2==0) ? n : n+1;
        double step_size = 2*M_PI / n_steps;

        for (int i = 0; i < n_steps; ++i) {
            for (int j = 0; j < n_steps/2.0; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n_steps)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n_steps)*step_size;

                // vertex i,j
                double vij_x = x(radius, phi_i, theta_j);
                double vij_y = y(radius, phi_i, theta_j);
                double vij_z = z(radius, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(radius, phi_ip1, theta_j);
                double vip1j_y = y(radius, phi_ip1, theta_j);
                double vip1j_z = z(radius, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(radius, phi_i, theta_jp1);
                double vijp1_y = y(radius, phi_i, theta_jp1);
                double vijp1_z = z(radius, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(radius, phi_ip1, theta_jp1);
                double vip1jp1_y = y(radius, phi_ip1, theta_jp1);
                double vip1jp1_z = z(radius, phi_ip1, theta_jp1);

                // add triangle
				float v1x = vip1j_x - vij_x, v1y = vip1j_y - vij_y, v1z = vip1j_z - vij_z;
				float v2x = vijp1_x - vij_x, v2y = vijp1_y - vij_y, v2z = vijp1_z - vij_z;
				float nx = v1y*v2z - v1z*v2y, ny = v1x*v2z - v1z*v2x, nz = v1x*v2y - v1y*v2x;
				float dist = sqrt(nx*nx + ny*ny + nz*nz);
				nx /= dist, ny /= dist, nz /= dist;
				if (i >= n_steps / 2) {
					nx *= -1;
					ny *= -1;
					nz *= -1;
				}
                add_vertex(coords, vij_x, vij_y, vij_z, nx, -ny, nz, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, nx, -ny, nz, r, g, b);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, nx, -ny, nz, r, g, b);

                // add triange
				v1x = vip1jp1_x - vijp1_x, v1y = vip1jp1_y - vijp1_y, v1z = vip1jp1_z - vijp1_z;
				v2x = vip1j_x - vijp1_x, v2y = vip1j_y - vijp1_y, v2z = vip1j_z - vijp1_z;
				nx = v1y*v2z - v1z*v2y, ny = v1x*v2z - v1z*v2x, nz = v1x*v2y - v1y*v2x;
				dist = -sqrt(nx*nx + ny*ny + nz*nz);
				nx /= dist, ny /= dist, nz /= dist;
				if (i >= n_steps / 2) {
					nx *= -1;
					ny *= -1;
					nz *= -1;
				}
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, nx, -ny, nz, r, g, b);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, nx, -ny, nz, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, nx, -ny, nz, r, g, b);
            }
        }
    }
};

class Torus {
    double x(float c, float a, float phi, float theta){
        return (c+a*cos(theta))*cos(phi);
    }

    double y(float c, float a, float phi, float theta){
        return (c+a*cos(theta))*sin(phi);
    }

    double z(float c, float a, float phi, float theta){
        return a*sin(theta);
    }

public:
    std::vector<float> coords;
    Torus(unsigned int n, float c, float a, float r, float g, float b) {

        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n)*step_size;

                // vertex i,j
                double vij_x = x(c, a, phi_i, theta_j);
                double vij_y = y(c, a, phi_i, theta_j);
                double vij_z = z(c, a, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(c, a, phi_ip1, theta_j);
                double vip1j_y = y(c, a, phi_ip1, theta_j);
                double vip1j_z = z(c, a, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(c, a, phi_i, theta_jp1);
                double vijp1_y = y(c, a, phi_i, theta_jp1);
                double vijp1_z = z(c, a, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(c, a, phi_ip1, theta_jp1);
                double vip1jp1_y = y(c, a, phi_ip1, theta_jp1);
                double vip1jp1_z = z(c, a, phi_ip1, theta_jp1);

                // add triangle
                add_vertex(coords, vij_x, vij_y, vij_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);

                // add triange
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
            }
        }
    }

};

#endif
