#version 330 core

out vec4 fragColor;
in vec2 ourTexCoord;
uniform sampler2D tex;

void main() {
    //fragColor = vec4(ourTexCoord, 0, 1);
    fragColor = texture(tex, ourTexCoord);
}
