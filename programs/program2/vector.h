#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>

class DLEL {
private:
	int origin;
	int face;
public:
	DLEL(int startPoint);
	int getOrigin();
	int getFace();
	void setFace(int newFace);
	DLEL* next;
	DLEL* last;
	DLEL* twin;
};

class Vector {
public:
	float x, y, z, w;
	DLEL* out;
	Vector();
	Vector(float xCoord, float yCoord, float zCoord, float wVal=1.0f);
	void normalize();
	void normalized(Vector* out);
	float determinant(float a, float b, float c, float d);
	void scale(float scalar);
	Vector& cross(Vector& v);
	Vector& operator+(Vector& v);
	Vector& operator-(Vector& v);
	bool operator==(Vector& v);
	std::ostream& operator<<(std::ostream& os);
};

#endif