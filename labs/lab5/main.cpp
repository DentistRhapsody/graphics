#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

const int shapeCount = 3;
int shapeToDraw = 0;
int lastShape = 0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, GLFWwindow *window) {
    Matrix trans;

    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;

    // ROTATE
    if (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
    else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
    else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
    else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
    else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
    else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
    // SCALE
    else if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
    else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
    // TRANSLATE
    else if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(-TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(TRANS, 0, 0); }
    else if (isPressed(window, ',')) { trans.translate(0,0,TRANS); }
    else if (isPressed(window, '.')) { trans.translate(0,0,-TRANS); }
	else {
		if (isReleased(window, GLFW_KEY_SPACE)) {
			if (lastShape == -1) {
				lastShape = 0;
				shapeToDraw = (shapeToDraw + 1) % 3;
				std::cout << shapeToDraw << std::endl;
			}
		}
		else if (isPressed(window, GLFW_KEY_SPACE)) {
			lastShape = -1;
		}
	}

    return trans * model;
}

void processInput(Matrix& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    model = processModel(model, window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "lab5", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }
    
	DiscoCube c;
	Cylinder c2(20, 1, .2, .4);
	Sphere c3(20, .5, 1, .2, .4);

    // copy vertex data
	GLuint VBOs[shapeCount];
	GLuint VAOs[shapeCount];
	std::vector<float> shapes[] = { c.coords, c2.coords, c3.coords };
	for (int i = 0; i < shapeCount; i++) {
		glGenBuffers(1, &VBOs[i]);
		glBindBuffer(GL_ARRAY_BUFFER, VBOs[i]);
		glBufferData(GL_ARRAY_BUFFER, shapes[i].size() * sizeof(float),
			&shapes[i][0], GL_STATIC_DRAW);

		// describe vertex layout
		glGenVertexArrays(1, &VAOs[i]);
		glBindVertexArray(VAOs[i]);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float),
			(void*)0);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float),
			(void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float),
			(void*)(6 * sizeof(float)));
		glEnableVertexAttribArray(2);
	}

    // setup projection
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

    // setup view
    Vector eye(0, 0, -2);
    Vector origin(0, 0, 0);
    Vector up(0, 1, 0);

    Matrix camera;
    camera.look_at(eye, origin, up);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // set the matrices
    Matrix model;

	// Lighting stuff!
	float ambientStrength = 0.1;
	GLfloat lightColor[3] = { 1.0f, 1.0f, 1.0f };
	GLfloat lightPos[3] = { 1.2f, 1.0f, 1.0f };
	Matrix tempM, tempM2, normalMatrix;

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(model, window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate shader
        shader.use();

        Uniform::set(shader.id(), "model", model);
        Uniform::set(shader.id(), "projection", projection);
        Uniform::set(shader.id(), "camera", camera);

		model.inverse(&tempM2);
		tempM2.transpose(&normalMatrix);

		GLuint normalV = glGetUniformLocation(shader.id(), "normalMatrix");
		GLuint ambientV = glGetUniformLocation(shader.id(), "ambient");
		GLuint lightPosV = glGetUniformLocation(shader.id(), "lightPos");
		GLuint lightColorV = glGetUniformLocation(shader.id(), "lightColor");
		GLuint viewPosV = glGetUniformLocation(shader.id(), "viewPos");
		GLuint shininessV = glGetUniformLocation(shader.id(), "shininess");
		glUniformMatrix4fv(normalV, 1, GL_FALSE, normalMatrix.values);
		glUniform1f(ambientV, 0.1f);
		glUniform3fv(lightPosV, 1, lightPos);
		glUniform3fv(lightColorV, 1, lightColor);
		glUniform3fv(viewPosV, 1, eye.values);
		glUniform1f(shininessV, 32.0f);

        // render the cube
		glBindVertexArray(VAOs[shapeToDraw]);
		glDrawArrays(GL_TRIANGLES, 0, shapes[shapeToDraw].size() * sizeof(float));
        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
