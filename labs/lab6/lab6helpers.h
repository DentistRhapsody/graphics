#include <cstdlib>
#include <vector>
#include <iostream>
#include <fstream>
#include <csci441/vector.h>

class HalfEdge;
class Vec;
class Face;
class DoublyLinkedList;

class HalfEdge {
	//HalfEdge* halfedge;
	HalfEdge* nextedge = NULL;
	HalfEdge* lastedge = NULL;
	HalfEdge* twin = NULL;
	Vec* origin = NULL;
	Face* face = NULL;

public:
	HalfEdge() {}

	//HalfEdge* getEdge() { return halfedge; }
	void setNext(HalfEdge* nextEdge) { nextedge = nextEdge; }
	HalfEdge* getNext() { return nextedge; }
	void setPrevious(HalfEdge* previousEdge) { lastedge = previousEdge; }
	HalfEdge* getPrevious() { return lastedge; }
	void setTwin(HalfEdge* twinEdge) { twin = twinEdge; }
	HalfEdge* getTwin() { return twin; }
	void setOrigin(Vec* originVector) { origin = originVector; }
	Vec* getOrigin() { return origin; }
	void setFace(Face* f) { face = f; }
	Face* getFace() { return face; }
};

class Vec: public Vector {
	HalfEdge* edge = NULL;
	
public:
	Vec(float x, float y, float z, float w = 1) : Vector(x, y, z, w) {}
	Vec(HalfEdge* e, float x, float y, float z, float w = 1) : Vector(x, y, z, w) {
		edge = e;
	}

	void addEdge(HalfEdge* he) { edge = he; }
	HalfEdge* getEdge() { return edge; }
};

class Face {
	HalfEdge* edge = NULL;
	Vector normal;

public:
	
	Face() : normal(Vector(0.0f, 0.0f, 0.0f, 1.0f)) {}
	Face(HalfEdge* incidentEdge) : normal(Vector(0.0f, 0.0f, 0.0f, 1.0f)) {
		setEdge(incidentEdge);
	}

	void setEdge(HalfEdge* e) { edge = e; }
	HalfEdge* getEdge() { return edge; }
	void setNormal(float x, float y, float z) {
		float x1 = x != -1 ? x : normal.x();
		float y1 = y != -1 ? y : normal.y();
		float z1 = z != -1 ? z : normal.z();
		normal = Vector(x, y, z).normalized();
	}
	Vector* getNormal() { return &normal; }
	float getNormalX() { return normal.x(); }
	float getNormalY() { return normal.y(); }
	float getNormalZ() { return normal.z(); }
	/*void addEdge(HalfEdge* he) { border.push_back(he); }
	std::vector<HalfEdge*> getBorder() { return border; }*/
};

class DoublyLinkedList {
	std::vector<Vec> vertices;
	std::vector<Face*> faces;

public:
	DoublyLinkedList(std::string filepath) {
		std::ifstream file(filepath);
		std::string line;
		if (file.is_open()) {
			while (getline(file, line)) {
				if (line.length() > 0 && line[0] == 'v') {
					int length = line.length();
					std::string s = line.substr(2, length);
					char* c;
					float x = strtof(s.c_str(), &c);
					float y = strtof(c, &c);
					float z = strtof(c, &c);
					addVertex(x, y, z);
				}
				else if (line.length() > 0 && line[0] == 'f') {
					int length = line.length();
					std::string s = line.substr(2, length);
					char* c;
					int pointOne = (int)strtof(s.c_str(), &c);
					int pointTwo = (int)strtof(c, &c);
					int pointThree = (int)strtof(c, &c);
					addFace(pointOne, pointTwo, pointThree);
				}
			}

			matchTwins();
		}
	}

	void addVertex(float x, float y, float z) { vertices.push_back(Vec(x, y, z)); }
	
	void addFace(int v1, int v2, int v3) {
		HalfEdge* e1 = addSegment(v1);
		HalfEdge* t1 = addSegment(v2);
		HalfEdge* e2 = addSegment(v2);
		HalfEdge* t2 = addSegment(v3);
		HalfEdge* e3 = addSegment(v3);
		HalfEdge* t3 = addSegment(v1);
		Face* f = new Face();
		f->setEdge(e1);
		e1->setNext(e2);
		e2->setNext(e3);
		e3->setNext(e1);
		t1->setNext(t3);
		t2->setNext(t1);
		t3->setNext(t2);
		e1->setPrevious(e3);
		e2->setPrevious(e1);
		e3->setPrevious(e2);
		t1->setPrevious(t2);
		t2->setPrevious(t3);
		t3->setPrevious(t1);
		e1->setTwin(t1);
		t1->setTwin(e1);
		e2->setTwin(t2);
		t2->setTwin(e2);
		e3->setTwin(t3);
		t3->setTwin(e3);
		Vector vn1 = vertices[v1 - 1] - vertices[v2 - 1];
		Vector vn2 = vertices[v2 - 1] - vertices[v3 - 1];
		Vector cross = vn1.cross(vn2).normalized();
		f->setNormal(cross.x(), cross.y(), cross.z());
		e1->setFace(f);
		e2->setFace(f);
		e3->setFace(f);
		e3->setFace(f);
		faces.push_back(f);
	}

	HalfEdge* addSegment(int origin) {
		HalfEdge* edge = new HalfEdge();
		Vec* vertex = &vertices[origin - 1];
		edge->setOrigin(vertex);
		if (vertex->getEdge() == nullptr)
			vertex->addEdge(edge);
		return edge;
	}

	void matchTwins() {
		for (int i = 0; i < faces.size() - 1; i++) {
			for (int j = i + 1; j < faces.size(); j++) {
				HalfEdge* fe1 = faces[i]->getEdge();
				HalfEdge* fe2 = faces[j]->getEdge();
				for (int a = 0; a < 3; a++) {
					for (int b = 0; b < 3; b++) {
						if (fe1->getOrigin() == fe2->getNext()->getOrigin() && fe1->getNext()->getOrigin() == fe2->getOrigin()) {
							fe1->getTwin()->getNext()->setPrevious(fe2->getTwin()->getPrevious());
							fe2->getTwin()->getNext()->setPrevious(fe1->getTwin()->getPrevious());
							fe1->getTwin()->getPrevious()->setNext(fe2->getTwin()->getNext());
							fe2->getTwin()->getPrevious()->setNext(fe1->getTwin()->getNext());
							fe1->setTwin(fe2);
							fe2->setTwin(fe1);
						}
						fe2 = fe2->getNext();
					}
					fe1 = fe1->getNext();
				}
			}
		}
	}

	std::vector<float> getCoords(bool smooth=false) {
		std::vector<float> out;
		for (int i = 0; i < faces.size(); i++) {
			Face* f = faces[i];
			HalfEdge* startingEdge = f->getEdge();
			HalfEdge* next = startingEdge->getNext();
			while (next != startingEdge) {
				out.push_back(next->getOrigin()->x());
				out.push_back(next->getOrigin()->y());
				out.push_back(next->getOrigin()->z());
				Vector* normals;
				if (smooth)
					normals = getSmoothNormals(next->getOrigin());
				else
					normals = &Vector(f->getNormalX(), f->getNormalY(), f->getNormalZ()).normalized();
				out.push_back(0.2f);
				out.push_back(0.2f);
				out.push_back(0.2f);
				out.push_back(normals->x());
				out.push_back(normals->y());
				out.push_back(normals->z());
				next = next->getNext();
			}
		}
		return out;
	}
	
	Vector* getSmoothNormals(Vec* v) {
		float out[3] = { 0.0f, 0.0f, 0.0f };
		int count = 0;
		HalfEdge* outEdge = v->getEdge();
		HalfEdge* nextEdge = v->getEdge();
		if (outEdge->getFace() == nullptr) {
			outEdge = outEdge->getTwin();
			nextEdge = nextEdge->getTwin();
		}
		do {
			Face* f = nextEdge->getFace();
			if (f != nullptr) {
				out[0] += f->getNormalX();
				out[1] += f->getNormalY();
				out[2] += f->getNormalZ();
				count++;
			}
			nextEdge = nextEdge->getTwin()->getNext();
		} while (outEdge != nextEdge);
		Vector* outV = new Vector(out[0], out[1], out[2]);
		return outV;
	}
};
