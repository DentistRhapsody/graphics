#ifndef SHAPE_H
#define SHAPE_H

#include <vector>
#include "vector.h"
#include "matrix.h"

struct Face {
public:
	Vector normal;
	DLEL* edge;
};

class Shape {
private:
	void loadCPP(std::string filepath);
	void addFace(int vertex1, int vertex2, int vertex3);
	void combineFaces();
public:
	std::string name;
	DLEL* edges;
	std::vector<Vector> vertices;
	std::vector<Face> faces;
	Matrix worldPos;

	Shape(std::string filepath);
	Shape(std::string filepath, std::string shapeName);
	std::vector<float> getVertices();
	std::vector<float> getFaces();
	std::vector<float> getFacesFull();
};


#endif