#ifndef MATRIX_H
#define MATRIX_H
#include <GLFW/glfw3.h>
#include "vector.h"

class Matrix {
public:
	GLfloat vals[16];
	Matrix();
	void identify();
	int z(int col, int row);
	float determinant();
	void rotateX(float theta);
	void rotateY(float theta);
	void rotateZ(float theta);
	void scale(float xScale, float yScale, float zScale);
	void translate(float xShift, float yShift, float zShift);
	Matrix transpose();
	Matrix invert();
	bool isInvertible();
	Matrix operator*(float scalar);
	Matrix operator*(Matrix& m);

	// Borrowed functions:
	void look_at(Vector& eye, Vector& target, Vector& up);
	void Matrix::ortho(float l, float r, float b, float t, float n, float f);
	void Matrix::perspective(float min_fov, float aspect_ratio, float n, float f);
};

#endif