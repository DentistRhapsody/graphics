#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include "shape.h"
#include "vector.h"

DLEL::DLEL(int startingPoint) : origin(startingPoint) { }

int DLEL::getOrigin() {
	return origin;
}

int DLEL::getFace() {
	return face;
}

void DLEL::setFace(int newFace) {
	face = newFace;
}

Shape::Shape(std::string filepath) {
	Shape(filepath, "new_shape");
}

Shape::Shape(std::string filepath, std::string shapeName) {
	name = shapeName;
	loadCPP(filepath);
}

void Shape::loadCPP(std::string filepath) {
	std::ifstream file(filepath);
	std::string line;
	if (file.is_open()) {
		while (std::getline(file, line)) {
			if (line[0] == 'v') {
				std::string s = line.substr(2, line.length());
				char* c;
				float x = strtof(s.c_str(), &c);
				float y = strtof(c, &c);
				float z = strtof(c, &c);
				vertices.push_back(Vector(x, y, z, 1));
			}
			else if (line[0] == 'f') {
				std::string s = line.substr(2, line.length());
				char* c;
				int x = (int)strtof(s.c_str(), &c);
				int y = (int)strtof(c, &c);
				int z = (int)strtof(c, &c);
				addFace(x, y, z);
			}
		}
		combineFaces();
		edges = faces[0].edge;
	}
}

void Shape::addFace(int vertex1, int vertex2, int vertex3) {
	DLEL* edge1 = new DLEL(vertex1 - 1);
	DLEL* edge2 = new DLEL(vertex2 - 1);
	DLEL* edge3 = new DLEL(vertex3 - 1);
	Face* f = new Face();
	Vector v1 = vertices[vertex1 - 1] - vertices[vertex2 - 1];
	Vector v2 = vertices[vertex2 - 1] - vertices[vertex3 - 1];
	v1.cross(v2).normalized(&f->normal);
	f->edge = edge1;
	int faceNum = faces.size();
	edge1->setFace(faceNum);
	edge2->setFace(faceNum);
	edge3->setFace(faceNum);
	edge1->next = edge2;
	edge2->next = edge3;
	edge3->next = edge1;
	edge1->last = edge3;
	edge2->last = edge1;
	edge3->last = edge2;
	vertices[vertex1 - 1].out = edge1;
	vertices[vertex2 - 1].out = edge2;
	vertices[vertex3 - 1].out = edge3;
	faces.push_back(*f);
}

void Shape::combineFaces() {
	for (int i = 0; i < faces.size(); i++) {
		for (int j = 0; j < faces.size(); j++) {
			DLEL* first = faces[i].edge;
			DLEL* second = faces[j].edge;
			for (int e1 = 0; e1 < 3; e1++) {
				for (int e2 = 0; e2 < 3; e2++) {
					if (first->getOrigin() == second->next->getOrigin() && first->next->getOrigin() == second->getOrigin()) {
						first->twin = second;
						second->twin = first;
					}
					second = second->next;
				}
				first = first->next;
			}
		}
	}
}

std::vector<float> Shape::getVertices() {
	std::vector<float> out;
	for (int i = 0; i < vertices.size(); i++) {
		out.push_back(vertices[i].x);
		out.push_back(vertices[i].y);
		out.push_back(vertices[i].z);
	}
	return out;
}

std::vector<float> Shape::getFaces() {
	std::vector<float> out;
	for (int i = 0; i < faces.size(); i++) {
		out.push_back(faces[i].edge->getOrigin());
		out.push_back(faces[i].edge->next->getOrigin());
		out.push_back(faces[i].edge->last->getOrigin());
	}
	return out;
}

std::vector<float> Shape::getFacesFull() {
	std::vector<float> out;
	for (int i = 0; i < faces.size(); i++) {
		out.push_back(faces[i].edge->getOrigin());
		out.push_back(faces[i].edge->next->getOrigin());
		out.push_back(faces[i].edge->last->getOrigin());
		out.push_back(0.5f);
		out.push_back(0.5f);
		out.push_back(0.5f);
		out.push_back(faces[i].normal.x);
		out.push_back(faces[i].normal.y);
		out.push_back(faces[i].normal.z);
	}
	return out;
}
