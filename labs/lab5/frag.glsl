#version 330 core
in vec3 ourColor;
in vec3 normal;
in vec3 fragPos;
uniform mat4 camera;
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform float ambient;
uniform float shininess;
uniform vec3 viewPos;

out vec4 fragColor;

void main() {
	vec3 amb = ambient * lightColor; //Ambient Lighting
	
	vec3 norm = normalize(normal); // Diffuse Lighting
	vec3 lightPos = viewPos * -lightPos;
	vec3 lightDir = normalize(lightPos - fragPos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * lightColor;
	
	vec3 viewDir = normalize(viewPos - fragPos); // Specular Lighting
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
	vec3 specular = 1 * spec * lightColor;
	
	vec3 colorVector = (amb + diffuse + specular) * ourColor; // Phong Lighting
    fragColor = vec4(colorVector, 1.0f);
	//fragColor = vec4(ourColor, 1.0f);
}
