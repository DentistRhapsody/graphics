#include <iostream>

#include "bitmap_image.hpp"
using namespace std;

/*
	Author: Christopher McCabe
	Date: 25 January 2018
	Aside from in-class notes, this answer on the math stackexchange really
	  helped me nail down the concepts used here:
	  https://math.stackexchange.com/questions/51326/determining-if-an-arbitrary-point-lies-inside-a-triangle-defined-by-three-points
*/
int main(int argc, char** argv) {
	
	// The following chunk of code reads in three points from the user.
	//   The points are stored in a 2D array: the three rows are the three
	//   points, the columns are x-position, y-position, red, green, blue.
	cout << "Enter 3 points (enter a point as x,y:r,g,b):" << endl;
	double points[3][5];
	for (int i = 0; i < 3; i++) {
		string newPt;
		cin >> newPt;
		string coords = newPt.substr(0, newPt.find(':'));
		string colors = newPt.substr(newPt.find(':') + 1);

		char x[256];
		strcpy(x, coords.substr(0, coords.find(',')).c_str());
		points[i][0] = atof(x);
		strcpy(x, coords.substr(coords.find(',') + 1).c_str());
		points[i][1] = atof(x);

		strcpy(x, colors.substr(0, colors.find(',')).c_str());
		points[i][2] = atof(x);
		strcpy(x, colors.substr(colors.find(',') + 1, colors.find(',', colors.find(','))).c_str());
		points[i][3] = atof(x);
		strcpy(x, colors.substr(colors.find(',', colors.find(',') + 1) + 1).c_str());
		points[i][4] = atof(x);
	}
	cout << "You entered:" << endl;
	for (int i = 0; i < 3; i++) {
		cout << points[i][0] << ", " << points[i][1] << " : " << points[i][2] << ", " << points[i][3] << ", " << points[i][4] << endl;
	}

	// create an image 640 pixels wide by 480 pixels tall
	bitmap_image image(640, 480);
	image.set_all_channels(0, 0, 0);
	image_drawer draw(image);
	rgb_t color = make_colour(255, 255, 255);

	// The bounding dimensions for the triangle.
	int xMin = min(points[0][0], min(points[1][0], points[2][0]));
	int xMax = max(points[0][0], max(points[1][0], points[2][0]));
	int yMin = min(points[0][1], min(points[1][1], points[2][1]));
	int yMax = max(points[0][1], max(points[1][1], points[2][1]));

	// Moving corners into the origin:
	int bx = points[1][0] - points[0][0];
	int by = points[1][1] - points[0][1];
	int cx = points[2][0] - points[0][0];
	int cy = points[2][1] - points[0][1];

	// Denominator to be used in later calculations.
	float denominator = bx * cy - by * cx;

	for (int x = xMin; x <= xMax; x++) {
		for (int y = yMin; y <= yMax; y++) {
			// Again, move corner to the origin:
			int px = x - points[0][0];
			int py = y - points[0][1];

			// These three floats are the weights for each dimension:
			float w1 = (px * (by - cy) + py * (cx - bx) + bx * cy - by * cx) / denominator;
			float w2 = (px * cy - py * cx) / denominator;
			float w3 = (py * bx - px * by) / denominator;

			// If all the weights are between zero and one, then the point is within the triangle.
			if (w1 >= 0 && w1 <= 1 && w2 >= 0 && w2 <= 1 && w3 >= 0 && w3 <= 1) {
				float cr = w1 * points[0][2] + w2 * points[1][2] + w3 * points[2][2];
				float cg = w1 * points[0][3] + w2 * points[1][3] + w3 * points[2][3];
				float cb = w1 * points[0][4] + w2 * points[1][4] + w3 * points[2][4];
				image.set_pixel(x, y, make_colour(cr * 255, cg * 255, cb * 255));
			}
		}
	}

	image.save_image("triangle.bmp");
	std::cout << "Success" << std::endl;
}
