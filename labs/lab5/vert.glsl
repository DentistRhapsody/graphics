#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec3 aColor;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;
uniform mat4 normalMatrix;

out vec3 normal;
out vec3 fragPos;
out vec3 ourColor;

void main() {
    ourColor = aColor;
	fragPos = vec3(model * vec4(aPos, 1.0));
	normal = mat3(normalMatrix) * aNormal;
    gl_Position = projection * camera * vec4(fragPos, 1.0);
}
