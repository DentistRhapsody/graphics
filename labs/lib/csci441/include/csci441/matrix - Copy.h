#ifndef _CSCI441_MATRIX_H_
#define _CSCI441_MATRIX_H_
#define	_USE_MATH_DEFINES
#include <cmath>
#include <math.h>

#include <sstream>

#include "vector.h"

class Matrix {
private:
    unsigned int idx(unsigned int r, unsigned int c) const {
        return r + c*4;
    }

public:
    float values[16];

    Matrix() {
        set_to_identity();
    };

<<<<<<< HEAD
	// Begin student-written code.
	// Much of my understanding of these came from https://www.mathsisfun.com/algebra/matrix-inverse-minors-cofactors-adjugate.html
	
	/* Returns the determinant of the passed double array as if it were a 3x3 matrix. */
	float determinant3(double m[9]) {
		return (m[0] * m[4] * m[8]) + (m[1] * m[5] * m[6]) + (m[2] * m[3] * m[7]) -
			(m[2] * m[4] * m[6]) - (m[0] * m[5] * m[7]) - (m[1] * m[3] * m[8]);
	}

	/* Returns the determinant of this 4x4 matrix.
	 * Hardcode provided by Drew Noakes: https://stackoverflow.com/questions/2922690/calculating-an-nxn-matrix-determinant-in-c-sharp/2922905#2922905
	 */
	float determinant4() {
		return values[12] * values[9] * values[6] * values[3] - values[8] * values[13] * values[6] * values[3] -
			values[12] * values[5] * values[10] * values[3] + values[4] * values[13] * values[10] * values[3] +
			values[8] * values[5] * values[14] * values[3] - values[4] * values[9] * values[14] * values[3] -
			values[12] * values[9] * values[2] * values[7] + values[8] * values[13] * values[2] * values[7] +
			values[12] * values[1] * values[10] * values[7] - values[0] * values[13] * values[10] * values[7] -
			values[8] * values[1] * values[14] * values[7] + values[0] * values[9] * values[14] * values[7] +
			values[12] * values[5] * values[2] * values[11] - values[4] * values[13] * values[2] * values[11] -
			values[12] * values[1] * values[6] * values[11] + values[0] * values[13] * values[6] * values[11] +
			values[4] * values[1] * values[14] * values[11] - values[0] * values[5] * values[14] * values[11] -
			values[8] * values[5] * values[2] * values[15] + values[4] * values[9] * values[2] * values[15] +
			values[8] * values[1] * values[6] * values[15] - values[0] * values[9] * values[6] * values[15] -
			values[4] * values[1] * values[10] * values[15] + values[0] * values[5] * values[10] * values[15];
	}

	/* Stores the minor matrix of this matrix in the passed matrix. */
	void minor(Matrix* m) {
		double t[9]; // This temp array is for determinant factoring.
		for (int row = 0; row < 4; row++) {
			for (int col = 0; col < 4; col++) {
				int tVals = 0;
				int x = 0;
				while (tVals < 9) {
					if (x % 4 != row && x / 4 != col)
						t[tVals++] = values[x];
					x++;
				}
				m->values[idx(row, col)] = determinant3(t);
			}
		}
	}

	/* Stores the cofactors of this matrix in the passed matrix. */
	void cofactor(Matrix* m) {
		for (int row = 0; row < 4; row++)
			for (int col = 0; col < 4; col++) {
				m->values[idx(row, col)] = values[idx(row, col)];
				if ((row + col) % 2 == 1)
					m->values[idx(row, col)] *= -1;
			}
	}

	/* Stores the transpose of this matrix in the passed matrix. */
	void transpose(Matrix* m) {
		for (int row = 0; row < 4; row++) {
			for (int col = 0; col < 4; col++) {
				m->values[idx(row, col)] = values[idx(col, row)];
			}
		}
	}

	/* Stores the inverse of this matrix in the passed matrix. */
	void inverse(Matrix* m) {
		if (!m->isInvertible()) {
			std::cout << "Not invertible" << std::endl;
			return;
		}
		float det = determinant4();
		Matrix minors, cofactors, adjoint;
		minor(&minors);
		minors.cofactor(&cofactors);
		cofactors.transpose(&adjoint);
		for (int i = 0; i < 16; i++)
			m->values[i] = adjoint.values[i] / det;
		/*std::cout << "++++" << std::endl << std::endl << to_string() << std::endl << 
			minors.to_string() << std::endl << cofactors.to_string() << std::endl << 
			adjoint.to_string() << std::endl << m->to_string() << std::endl << "----" << std::endl;*/
	}

	bool isInvertible() {
		return determinant4() != 0;
	}

	// End student-written code.
=======
    float operator()(int row, int col) const {
        return values[idx(row, col)];
    }
>>>>>>> a40443401c8fa409536109ca17fe40c4571b73cc

    void scale(float x, float y, float z) {
        set_to_identity();
        values[idx(0,0)] = x;
        values[idx(1,1)] = y;
        values[idx(2,2)] = z;
    }

    void rotate_x(float theta) {
        double theta_radians = theta*M_PI/180.0;

        set_to_identity();
        values[idx(1,1)] = std::cos(theta_radians);
        values[idx(1,2)] = -std::sin(theta_radians);
        values[idx(2,1)] = std::sin(theta_radians);
        values[idx(2,2)] = std::cos(theta_radians);
    }

    void rotate_y(float theta) {
        double theta_radians = theta*M_PI/180.0;

        set_to_identity();
        values[idx(0,0)] = std::cos(theta_radians);
        values[idx(0,2)] = std::sin(theta_radians);
        values[idx(2,0)] = -std::sin(theta_radians);
        values[idx(2,2)] = std::cos(theta_radians);
    }

    void rotate_z(float theta) {
        double theta_radians = theta*M_PI/180.0;

        set_to_identity();
        values[idx(0,0)] = std::cos(theta_radians);
        values[idx(0,1)] = -std::sin(theta_radians);
        values[idx(1,0)] = std::sin(theta_radians);
        values[idx(1,1)] = std::cos(theta_radians);
    }

    void translate(float x, float y, float z) {
        set_to_identity();
        values[idx(0,3)] = x;
        values[idx(1,3)] = y;
        values[idx(2,3)] = z;
    }

    void look_at(const Vector& eye, const Vector& target, const Vector& up) {
        Vector gaze = target-eye;
        Vector w = gaze.normalized().scale(-1);
        Vector u = up.cross(w).normalized();
        Vector v = w.cross(u);

        Matrix camera;
        Vector* basis[3] = {&u, &v, &w};
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                camera.values[idx(i,j)] = basis[i]->coord(j);
            }
        }

        Matrix translate;
        translate.translate(-eye.x(), -eye.y(), -eye.z());

        set_to_identity();
        mult(values, camera.values, translate.values);
    }

    void ortho(float l, float r, float b, float t, float n, float f) {
        set_to_identity();
        values[idx(0,0)] = 2 / (r-l);
        values[idx(0,3)] = -(r+l) / (r-l);

        values[idx(1,1)] = 2 / (t-b);
        values[idx(1,3)] = -(t+b) / (t-b);

        values[idx(2,2)] = -2 / (f-n);
        values[idx(2,3)] = -(f+n) / (f-n);
    }

    void perspective(float min_fov, float aspect_ratio, float n, float f) {
        set_to_identity();

        double theta = min_fov*M_PI/180.0;

        values[idx(0,0)] = 1 / (aspect_ratio*tan(theta));
        values[idx(1,1)] = 1 / (tan(theta));
        values[idx(2,2)] = -(f+n) / (f-n);
        values[idx(2,3)] = (-2*f*n) / (f-n);
        values[idx(3,2)] = -1;
        values[idx(3,3)] = 0;
    }

    void perspective(float l, float r, float b, float t, float n, float f) {
        set_to_identity();
        values[idx(0,0)] = 2*n / (r-l);
        values[idx(0,2)] = (r+l) / (r-l);

        values[idx(1,1)] = 2*n / (t-b);
        values[idx(1,2)] = (t+b) / (t-b);

        values[idx(2,2)] = -(f+n) / (f-n);
        values[idx(2,3)] = (-2*f*n) / (f-n);

        values[idx(3,2)] = -1;
        values[idx(3,3)] = 0;
    }

    Matrix operator*(const Matrix& m) const {
        Matrix ret;
        mult(ret.values, values, m.values);
        return ret;
    }

    std::string to_string()  const {
        std::ostringstream os;
        for (int row = 0; row < 4; ++row) {
            for (int col = 0; col < 4; ++col) {
                os << values[idx(row, col)] << " ";
            }
            os << std::endl;
        }
        return os.str();
    }

    friend std::ostream& operator<<(std::ostream& os, const Matrix& m) {
        os << m.to_string();
        return os;
    }

private:

    double dot(const float* m1, unsigned int row, const float* m2, int col) const {
        double dot = 0;
        for (int i = 0; i < 4; ++i) {
            dot += m1[idx(row,i)]*m2[idx(i,col)];
        }
        return dot;
    }

    void mult(float* target, const float* const m1, const float* const m2) const {
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                target[idx(i,j)] = dot(m1, i, m2, j);
            }
        }
    }

    void set_to_identity() {
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                values[idx(i,j)] = i==j;
            }
        }
    }
};

#endif
