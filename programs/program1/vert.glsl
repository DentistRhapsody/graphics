#version 330 core

layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;

out vec3 ourColor;

void main() {
    ourColor = aColor;
	gl_Position = projection * camera * vec4(vec3(model * vec4(aPos, 0.0, 1.0)), 1.0);
}
