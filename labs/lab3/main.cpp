#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>

int mode = 0;  // The four modes 0-3: static, rotating, offset rotating, scaling.
int keyState = GLFW_RELEASE;  // To allow for state change on key release.

GLfloat transform[] = {  // The transform matrix.
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f
};

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void changeMode() {
	mode++;
	if (mode >= 4)
		mode = 0;
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS && keyState == GLFW_RELEASE) {
		keyState = GLFW_PRESS;
	}
	else if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE && keyState == GLFW_PRESS) {
		changeMode();
		keyState = GLFW_RELEASE;
	}
}

int main(void) {
    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
	Shader shader("../vert.glsl", "../frag.glsl");

	/* Loop until the user closes the window */
	glBindVertexArray(VAO[0]);
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();

        /** Part 2 animate and scene by updating the transformation matrix */
		GLuint sLoc = glGetAttribLocation(shader.id(), "aPos");
		if (sLoc == -1)
			std::cout << "Help! Vertex matrix not found! " << sLoc << std::endl;
		GLuint sTrn = glGetUniformLocation(shader.id(), "transform");
		if (sTrn == -1)
			std::cout << "Help! Transform matrix not found! " << sTrn << std::endl;
		
		GLfloat time = glfwGetTime();
		if (mode == 0) {  // Static.
			transform[0] = 1.0f;
			transform[1] = 0;
			transform[4] = 0;
			transform[5] = 1.0f;
		}
		if (mode == 1 || mode == 2) {  // Spinning.
			transform[0] = GLfloat(cos(time));
			transform[1] = GLfloat(-sin(time));
			transform[4] = GLfloat(sin(time));
			transform[5] = GLfloat(cos(time));
		}
		if (mode == 2) {  // Spinning Offset.
			transform[13] = 0.25f;
			transform[12] = 0.25f;
		}
		if (mode == 3) {  // Scaling.
			transform[13] = 0;
			transform[12] = 0;
			transform[0] = GLfloat(cos(time) / 4 + 0.75);
			transform[1] = 0;
			transform[4] = 0;
			transform[5] = GLfloat(cos(time) / 4 + 0.75);
		}
		glUniformMatrix4fv(sTrn, 1, GL_FALSE, transform);  // Overwrite the transform matrix!

        // draw our triangles
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
