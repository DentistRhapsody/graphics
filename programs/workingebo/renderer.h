#ifndef _CSCI441_RENDERER_H_
#define _CSCI441_RENDERER_H_

#include <csci441/matrix3.h>

class Renderer {
    Matrix3 itModel_tmp;
public:

    void render(const Camera& camera, ChrisModel& m, const Vector& light) {
        itModel_tmp.inverse_transpose(m.model);

        m.shader.use();
        Uniform::set(m.shader.id(), "model", m.model);
        Uniform::set(m.shader.id(), "projection", camera.projection);
        Uniform::set(m.shader.id(), "camera", camera.look_at());
        Uniform::set(m.shader.id(), "eye", camera.eye);
        Uniform::set(m.shader.id(), "lightPos", light);
        Uniform::set(m.shader.id(), "itModel", itModel_tmp);

        // render the cube
        glBindVertexArray(m.vao);
		/*std::vector<int> x = m.faces;
		for (int i = 0; i < x.size(); i++) {
			if (i % 3 == 0)
				std::cout << std::endl;
			std::cout << "\t" << x[i] << " ";
		}
		std::cout << std::endl << std::endl;*/
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
    }
};

#endif
