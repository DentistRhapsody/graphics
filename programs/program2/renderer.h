#ifndef RENDERER_H_
#define RENDERER_H_

#include "matrix.h"

class Uniform {
public:
	static void set(GLuint shader, const std::string &name, const Matrix& m) {
		GLuint loc = location(shader, name);
		glUniformMatrix4fv(loc, 1, GL_FALSE, m.vals);
	}

	static void set(GLuint shader, const std::string &name, const Vector& v) {
		GLuint loc = location(shader, name);
		glUniform3f(loc, v.x, v.y, v.z);
	}

private:

	static GLuint location(GLuint shader, const std::string &name) {
		return glGetUniformLocation(shader, name.c_str());
	}
};

class Renderer {
    Matrix itModel_tmp;
public:

    void render(Camera& camera, Shader& s, Vector& light) {
        itModel_tmp = s.getShape()->worldPos.invert(); // This line has potential for failure

        s.use();
        Uniform::set(s.id(), "model", s.getShape()->worldPos);
        Uniform::set(s.id(), "projection", camera.projection);
        Uniform::set(s.id(), "camera", camera.look_at());
        Uniform::set(s.id(), "eye", camera.eye);
        Uniform::set(s.id(), "lightPos", light);
        Uniform::set(s.id(), "itModel", itModel_tmp);

        glBindVertexArray(s.vao);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }
};

#endif
