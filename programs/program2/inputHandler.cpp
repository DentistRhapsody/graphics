#include <iostream>
#include "inputHandler.h"

IO::IO(GLFWwindow& window) : window(&window) { }

// Reworking this method and whole class to not rely on function passing.
Matrix IO::update() {
	Matrix out;


	for (int i = 0; i < 5; i++) {
		GLuint state = glfwGetKey(window, keys[i]);
		if (state != GLFW_RELEASE)
			for (int j = 0; j < holdEvents[i].size(); j++) {
				holdEvents[i][j];
			}
		if (state != GLFW_RELEASE && keyStates[i] == GLFW_RELEASE)
			for (int j = 0; j < pressEvents[i].size(); j++)
				pressEvents[i][j];
		keyStates[i] = state;
	}
	return out;
}