#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "linkedlist.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

const int MODE_VIEW = 0;
const int MODE_STAMP = 1;
const int MODE_USERDEF = 2;
const int MODE_GROUP = 3;
const int MODE_MODIFY = 4;
const int numModes = 5;

// Setup the shape arrays.
std::vector<Shape> shapes;

Matrix camera;
Matrix projection;

// keys 1, 2, 3, 4, 5, space
GLint keyStates[] = { GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE };
int mode = 0;
int lastMode = 0;
bool mouseClicking = false;
double lastMousePos[] = { 0, 0 };
int stampShape = 0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) { return glfwGetKey(window, key) == GLFW_PRESS; }
bool isReleased(GLFWwindow *window, int key) { return glfwGetKey(window, key) == GLFW_RELEASE; }
bool isMousePressed(GLFWwindow *window) { return GLFW_PRESS == glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT); }
bool isMouseReleased(GLFWwindow *window) { return GLFW_RELEASE == glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT); }

bool isTapped(GLFWwindow *window, int key) {
	GLint keys[] = { GLFW_KEY_1, GLFW_KEY_2, GLFW_KEY_3, GLFW_KEY_4, GLFW_KEY_5, GLFW_KEY_SPACE };
	for (int i = 0; i < 6; i++) {
		if (key == keys[i]) {
			bool p = keyStates[i] == GLFW_RELEASE;
			bool s = isPressed(window, keys[i]);
			if (p && s) {
				keyStates[i] = GLFW_PRESS;
				return true;
			}
			else if (!p && !s)
				keyStates[i] = GLFW_RELEASE;
		}
	}
	return false;
}

void getMousePos(GLFWwindow *window, double& x, double& y) { 
	double x1, y1;
	glfwGetCursorPos(window, &x1, &y1);
	x = x1 / (SCREEN_WIDTH / 2) * (float)SCREEN_WIDTH / SCREEN_HEIGHT;
	y = -y1 / (SCREEN_HEIGHT / 2);
}

void updateMousePos(GLFWwindow *window) {
	double x, y;
	getMousePos(window, x, y);
	lastMousePos[0] = x;
	lastMousePos[1] = y;
}

Matrix processViewport(const Matrix& model, GLFWwindow *window) {
	Matrix trans;
	const float SCALE = .005;

	if (mode == MODE_VIEW) {
		if (isPressed(window, '-')) { trans.scale(1 - SCALE, 1 - SCALE, 1); }
		else if (isPressed(window, '=')) { trans.scale(1 + SCALE, 1 + SCALE, 1); }

		if (isMousePressed(window)) {  // Drag the viewport.
			double x, y;
			getMousePos(window, x, y);
			if (!mouseClicking) {
				lastMousePos[0] = x;
				lastMousePos[1] = y;
				mouseClicking = true;
			}
			double dx = x - lastMousePos[0], dy = y - lastMousePos[1];
			trans.translate(dx, dy, 0);
			lastMousePos[0] = x;
			lastMousePos[1] = y;
		}
	}
	return trans * model;
}

void hugMouse(Matrix& model, GLFWwindow *window) {
	Matrix trans;
	double x, y;
	getMousePos(window, x, y);
	double dx = x - lastMousePos[0], dy = y - lastMousePos[1];
	double fov = SCREEN_WIDTH * 1.0 / SCREEN_HEIGHT;
	trans.translate(-dx * fov, dy * fov, 0);
	lastMousePos[0] = x;
	lastMousePos[1] = y;
	model = trans * model;
}

/* Adds a shape to the provided list of Shapes and VAOs. */
void addShape(Shape &s, std::vector<Shape> &shapeList) {
	GLuint VAO;
	GLuint VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, s.coords.size() * sizeof(float), &s.coords[0], GL_STATIC_DRAW);

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
		(void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
		(void*)(2 * sizeof(float)));
	glEnableVertexAttribArray(1);

	s.VAO = VAO;
	shapeList.push_back(s);
}

void changeMode(GLFWwindow *window, int newMode) {
	bool modeChanged = mode != newMode;
	if (!modeChanged)
		return;
	lastMode = mode;
	if (newMode > numModes | newMode < 0)
		mode = MODE_VIEW;
	else
		mode = newMode;
	if (mode == MODE_STAMP) {
		updateMousePos(window);
		Shape s = Triangle();
		stampShape = shapes.size();
		addShape(s, shapes);
	}
}

void processInput(Matrix& model, GLFWwindow *window) {
    if (isTapped(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
	if (isTapped(window, GLFW_KEY_SPACE)) {
		if (mode != MODE_VIEW)
			changeMode(window, MODE_VIEW);
		else
			changeMode(window, lastMode);
	}
	if (isTapped(window, GLFW_KEY_1))
		changeMode(window, MODE_VIEW);
	if (isTapped(window, GLFW_KEY_2))
		changeMode(window, MODE_STAMP);
	if (isTapped(window, GLFW_KEY_3))
		changeMode(window, MODE_USERDEF);
	if (isTapped(window, GLFW_KEY_4))
		changeMode(window, MODE_GROUP);
	if (isTapped(window, GLFW_KEY_5))
		changeMode(window, MODE_MODIFY);
    model = processViewport(model, window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Project 1", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

	// Shape s = Circle();
	// addShape(s, shapes, VAOs);
	Shader shader("../vert.glsl", "../frag.glsl");

    // setup projection
	float r = (float)SCREEN_WIDTH / SCREEN_HEIGHT;
	std::cout << r << std::endl;
	float t = 1;
    projection.ortho(-r, r, -t, t, 0.1, 100);

    // setup view
    Vector eye(0, 0, -2);
    Vector origin(0, 0, 0);
    Vector up(0, 1, 0);

    camera.look_at(eye, origin, up);

    /*// create the shaders
    shaders.push_back(Shader("../vert.glsl", "../frag.glsl"));

    // setup the textures
    shaders[0].use();

    // set the matrices
    Matrix model;*/
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
		if (mouseClicking && !isMousePressed(window))
			mouseClicking = false;
        processInput(camera, window);

        /* Render here */

		std::cout << shapes.size() << std::endl;
        switch (mode) {
		case MODE_STAMP:
			//shaders[stampShape].shape.model.values[0] = lastMousePos[0];
			//shaders[stampShape].shape.model.values[5] = lastMousePos[1];
			//shaders[stampShape].shape.model.translate(lastMousePos[0], lastMousePos[1], 0);
			// std::cout << shaders[stampShape].shape.model << std::endl;
			hugMouse(shapes[stampShape].model, window);
		}
		shader.use();

		for (int s = 0; s < shapes.size(); s++) {

			glBindVertexArray(shapes[s].VAO);
			glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			Uniform::set(shader.id(), "model", shapes[s].model);
			Uniform::set(shader.id(), "projection", projection);
			Uniform::set(shader.id(), "camera", camera);

			glDrawArrays(GL_TRIANGLES, 0, shapes[s].coords.size() * sizeof(float));
		}

        // render the cube
		/* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
