#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/matrix_interpolation.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/compatibility.hpp>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

struct Keyframe {
    Vector from;
    Vector to;
    float t;
};

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

void processInput(GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

void getKeyFrame(Keyframe& keyframe,
        float time, std::vector<Vector> keyframes, int loopLength) {
    float loopTime = fmod(time, loopLength);

    // get the number of key frames
    size_t numFrames = keyframes.size();

    // Convert time to a value between 0 and 1
    // at 0 we're at the beginning of our rotations
    // array, and at 1 we've reach the last one
    float tFrame = fmin(loopTime/loopLength,1);

    // Get two indices into our rotations array
    // that represent our current animation
    unsigned int fromIndex = tFrame*(numFrames);

    // Since we want our animation to cycle
    // mod by the number of frames to
    // interpolate from the first frame to the last frame
    unsigned int toIndex = (fromIndex+1) % numFrames;

    // get the time per frame we will need this too know
    // this for interpolating between frames
    float timePerFrame = loopLength / (float)(numFrames);

    /* fill key frame */

    // compute the amount of time to interpolate between two frames
    keyframe.t = (loopTime - fromIndex*timePerFrame) / timePerFrame;

    // set the values for the two frames
    keyframe.from = keyframes[fromIndex];
    keyframe.to = keyframes[toIndex];
}


Matrix getModelMatrixLerp(const Vector& from, const Vector& to, float t) {
    // From Matrix math.
	glm::mat4 fromXMatrix(0.0f);
	fromXMatrix[0] = glm::vec4(1, 0, 0, 0);
	fromXMatrix[1] = glm::vec4(0, cos(from.x()), -sin(from.x()), 0);
	fromXMatrix[2] = glm::vec4(0, sin(from.x()), cos(from.x()), 0);

	glm::mat4 fromYMatrix(0.0f);
	fromYMatrix[0] = glm::vec4(cos(from.y()), 0, sin(from.y()), 0);
	fromYMatrix[1] = glm::vec4(0, 1, 0, 0);
	fromYMatrix[2] = glm::vec4(-sin(from.y()), 0, cos(from.y()), 0);

	glm::mat4 fromZMatrix(0.0f);
	fromZMatrix[0] = glm::vec4(cos(from.z()), -sin(from.z()), 0, 0);
	fromZMatrix[1] = glm::vec4(sin(from.z()), cos(from.z()), 0, 0);
	fromZMatrix[2] = glm::vec4(0, 0, 1, 0);

	glm::mat4 fromMatrix = fromZMatrix * fromYMatrix * fromXMatrix;

	//To Matrix math.
	glm::mat4 toXMatrix(0.0f);
	toXMatrix[0] = glm::vec4(1, 0, 0, 0);
	toXMatrix[1] = glm::vec4(0, cos(to.x()), -sin(to.x()), 0);
	toXMatrix[2] = glm::vec4(0, sin(to.x()), cos(to.x()), 0);

	glm::mat4 toYMatrix(0.0f);
	toYMatrix[0] = glm::vec4(cos(to.y()), 0, sin(to.y()), 0);
	toYMatrix[1] = glm::vec4(0, 1, 0, 0);
	toYMatrix[2] = glm::vec4(-sin(to.y()), 0, cos(to.y()), 0);

	glm::mat4 toZMatrix(0.0f);
	toZMatrix[0] = glm::vec4(cos(to.z()), -sin(to.z()), 0, 0);
	toZMatrix[1] = glm::vec4(sin(to.z()), cos(to.z()), 0, 0);
	toZMatrix[2] = glm::vec4(0, 0, 1, 0);

	glm::mat4 toMatrix = toZMatrix * toYMatrix * toXMatrix;

	// Interpolates the values into a new matrix to be returned.
	glm::mat4 outMatrix(0.0f);
	for (int i = 0; i < 3; i++) {
		outMatrix[i].x = fromMatrix[i].x * (1 - t) + toMatrix[i].x * t;
		outMatrix[i].y = fromMatrix[i].y * (1 - t) + toMatrix[i].y * t;
		outMatrix[i].z = fromMatrix[i].z * (1 - t) + toMatrix[i].z * t;
	}
	outMatrix[3].w = 1;  // It's a position, not a motion, so the bottom-right value should be 1.

    return Matrix(outMatrix);
}


Matrix getModelMatrixEuler(const Vector& from, const Vector& to, float t) {
    // Interpolated vector.
	glm::vec3 inter(from.x() * (1 - t) + to.x() * t, from.y() * (1 - t) + to.y() * t, from.z() * (1 - t) + to.z() * t);

	glm::mat4 xMatrix(0.0f);  // X values.
	xMatrix[0] = glm::vec4(1, 0, 0, 0);
	xMatrix[1] = glm::vec4(0, cos(inter.x), -sin(inter.x), 0);
	xMatrix[2] = glm::vec4(0, sin(inter.x), cos(inter.x), 0);

	glm::mat4 yMatrix(0.0f);  // Y values.
	yMatrix[0] = glm::vec4(cos(inter.y), 0, sin(inter.y), 0);
	yMatrix[1] = glm::vec4(0, 1, 0, 0);
	yMatrix[2] = glm::vec4(-sin(inter.y), 0, cos(inter.y), 0);

	glm::mat4 zMatrix(0.0f);  // Z values.
	zMatrix[0] = glm::vec4(cos(inter.z), -sin(inter.z), 0, 0);
	zMatrix[1] = glm::vec4(sin(inter.z), cos(inter.z), 0, 0);
	zMatrix[2] = glm::vec4(0, 0, 1, 0);

	glm::mat4 outMatrix = zMatrix * yMatrix * xMatrix;  // Combined rotation matrix.
	outMatrix[3].w = 1;  // It's a position, not a motion, so the bottom-right value should be 1.

	return Matrix(outMatrix);
}


Matrix getModelMatrixQuat(const Vector& from, const Vector& to, float t) {
    // The quaternion equivalent of the 'from' euler values.
	glm::quat fromQ = glm::quat(glm::vec3(from.x(), from.y(), from.z()));
	// ...and for the 'to' euler values.
	glm::quat toQ = glm::quat(glm::vec3(to.x(), to.y(), to.z()));

	// Interpolate and return the matrix equivalent.
	glm::quat out = glm::slerp(fromQ, toQ, t);
    return Matrix(glm::toMat4(out));
}


int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // create obj
    Model obj(
            DiscoCube().coords,
            Shader("../vert.glsl", "../frag.glsl"));

    // make a floor
    Model floor(
            DiscoCube().coords,
            Shader("../vert.glsl", "../frag.glsl"));
    Matrix floor_trans, floor_scale;
    floor_trans.translate(0, -6, 0);
    floor_scale.scale(100, 1, 100);
    floor.model = floor_trans*floor_scale;

    // setup camera
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

    Camera camera;
    camera.projection = projection;
    camera.eye = Vector(0, 0, 5);
    camera.origin = Vector(0, 0, 0);
    camera.up = Vector(0, 1, 0);

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;

    // set the light position
    Vector lightPos(3.75f, 3.75f, 4.0f);

    // rotations is an array of key frame Euler angles
    // feel free to change or add more key frames to the rotations
    // array.
    std::vector<Vector> rotations;
    rotations.push_back(Vector(0, 0, 0));
    rotations.push_back(Vector(0, 0, 0));
    rotations.push_back(Vector(-M_PI/3, -M_PI/2, 2*M_PI));
    rotations.push_back(Vector(M_PI/3, M_PI/2, -M_PI));
    rotations.push_back(Vector(M_PI/3, M_PI/2, -M_PI));

    int loopLength = rotations.size();

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        float time = glfwGetTime();

        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render the object and the floor
        renderer.render(camera, floor, lightPos);

        // render the cubes
        Matrix trans;
        Keyframe keyframe;
        getKeyFrame(keyframe, time, rotations, loopLength);

        // matrix lerp
        trans.translate(-2,0,0);
        obj.model =  trans * getModelMatrixLerp(keyframe.from, keyframe.to, keyframe.t);
        renderer.render(camera, obj, lightPos);

        // euler angles
        trans.translate(0,0,0);
        obj.model = trans * getModelMatrixEuler(keyframe.from, keyframe.to, keyframe.t);
        renderer.render(camera, obj, lightPos);

        // quaternions
        trans.translate(2,0,0);
        obj.model = trans * getModelMatrixQuat(keyframe.from, keyframe.to, keyframe.t);
        renderer.render(camera, obj, lightPos);

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
