#include <string>
#include <sstream>
#include <fstream>
#include <glad\glad.h>
#include "shader.h";

GLuint Shader::id() { return _id; }
void Shader::use() { glUseProgram(id()); }
Shape* Shader::getShape() { return &shape; }

Shader::Shader(std::string vertPath, std::string fragPath, Shape& s) : shape(s) {
	GLuint vShader = createShader(vertPath, GL_VERTEX_SHADER);
	GLuint fShader = createShader(fragPath, GL_FRAGMENT_SHADER);

	_id = createShaderProgram(vShader, fShader);
	glDeleteShader(vShader);
	glDeleteShader(fShader);

	// I wrote almost exclusively the following few lines of code in this file
	std::vector<float> vertices = s.getVertices();
	std::vector<float> indices = s.getFacesFull();
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);
	glBindVertexArray(vao);
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(&vertices[0]), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(&indices[0]), &indices[0], GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3*sizeof(float)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

GLuint Shader::createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
	GLuint program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	int success;
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		char infoLog[512];
		glGetProgramInfoLog(program, 512, NULL, infoLog);
		std::cerr << "ERROR::PROGRAM:COMPILATION_FAILED\n"
			<< infoLog << std::endl;
	}

	return program;
}

GLuint Shader::createShader(const std::string& fileName, GLenum shaderType) {
	std::ifstream stream(fileName);
	std::stringstream buffer;
	buffer << stream.rdbuf();
	stream.close();

	std::string source = buffer.str();
	std::cout << "Source:" << std::endl;
	std::cout << source << std::endl;

	GLuint shader = glCreateShader(shaderType);
	const char* src_ptr = source.c_str();
	glShaderSource(shader, 1, &src_ptr, NULL);
	glCompileShader(shader);

	int success;
	char infoLog[512];
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(shader, 512, NULL, infoLog);
		std::cerr << "ERROR::SHADER::" << "something"
			<< "::COMPILATION_FAILED\n"
			<< infoLog << std::endl;
	}
	return shader;
}