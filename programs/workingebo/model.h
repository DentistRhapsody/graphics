#ifndef _CSCI441_MODEL_H_
#define _CSCI441_MODEL_H_

#include <cstdlib>
#include <vector>

class ChrisModel {
public:
	GLuint vbo;
	GLuint vao;
	GLuint ebo;
	Shader shader;
	Matrix model;
	std::vector<float> vertices;
	std::vector<unsigned int> faces;
	int size;

	ChrisModel (const std::vector<float>& vertices, const std::vector<unsigned int>& faces, Shader& shader_in) : shader(shader_in), vertices(vertices), faces(faces) {
		float vs[48];
		for (int i = 0; i < vertices.size(); i++)
			vs[i] = vertices[i];
		unsigned int fs[36];
		for (int i = 0; i < faces.size(); i++)
			fs[i] = faces[i];
		
		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbo);
		glGenBuffers(1, &ebo);
		glBindVertexArray(vao);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		//glBufferData(GL_ARRAY_BUFFER, sizeof(vertices.data()), vertices.data(), GL_STATIC_DRAW);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vs), vs, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		//glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(faces.data()), faces.data(), GL_STATIC_DRAW);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(fs), fs, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3*sizeof(float)));
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
};

class Model {

public:
    GLuint vbo;
    GLuint vao;
    Shader shader;
    Matrix model;
    int size;

    //template <typename Coords>
    //Model(const Coords& coords, const Shader& shader_in) : shader(shader_in) {
	Model (std::vector<float>& coords, Shader& shader_in) : shader(shader_in) {
        size = coords.size()*sizeof(float);

        // copy vertex data
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, size, coords.data(), GL_STATIC_DRAW);

        // describe vertex layout
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(0*sizeof(float)));
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(3*sizeof(float)));
        glEnableVertexAttribArray(1);

        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(6*sizeof(float)));
        glEnableVertexAttribArray(2);
    }
};

#endif
