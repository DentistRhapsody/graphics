#define	_USE_MATH_DEFINES

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

// Keys are [0]:w, [1]:s, [2]:up, [3]:down, [4]:left, [5]:right, [6]:,, [7]:., [8]:-, [9]:=, [10]:u, [11]:i, [12]:o, [13]:p, [14]:[, [15]:], [16]:\.
GLint keys[18] = {
	GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE,
	GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE,
	GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE, GLFW_RELEASE,
	GLFW_RELEASE, GLFW_RELEASE
};
GLint keyNames[17] = {
	GLFW_KEY_W, GLFW_KEY_S, GLFW_KEY_UP, GLFW_KEY_DOWN, GLFW_KEY_LEFT, GLFW_KEY_RIGHT,
	GLFW_KEY_COMMA, GLFW_KEY_PERIOD, GLFW_KEY_MINUS, GLFW_KEY_EQUAL, GLFW_KEY_U,
	GLFW_KEY_I, GLFW_KEY_O, GLFW_KEY_P, GLFW_KEY_LEFT_BRACKET, GLFW_KEY_RIGHT_BRACKET,
	GLFW_KEY_BACKSLASH
};

/* A struct for each space containing a translation, rotation, and scale matrix.
 *   Also includes the current roll/pitch/yaw as a misnamed variable "quaternion",
 *   the most recent version of the culminated transformation matrix as "val", and
 *   the string name of this space (used for bug testing).
 */
struct Spaces {
	GLfloat translation[16];
	GLfloat rotation[16];
	GLfloat quaternion[4] = { 0, 0, 0, 1 };
	GLfloat scale[16];
	GLfloat val[16];
	std::string name;
};
// The Four spaces we use: Model, View, and either Orthographic or Perspective.
Spaces projectionSpace, orthographSpace, viewSpace, modelSpace;

// The Identity matrix. Used for testing.
GLfloat identity[16] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

// False if currently using the Orthographic view.
bool projectionActive = true;

// A flag to prevent the view swapping key from toggling rapidly.
bool projectionPressed = false;

/* Multiplies two 4x4 matricies together and puts the result in the out variable. */
void matrixMultiply(GLfloat* first, GLfloat* second, GLfloat* out);
void matrixMultiply(GLfloat* first, GLfloat* second, GLfloat* out, bool print);
void matrixMultiply(GLfloat* first, GLfloat* second, GLfloat* out) { matrixMultiply(first, second, out, false); }
void matrixMultiply(GLfloat* first, GLfloat* second, GLfloat* out, bool print) {
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			out[i * 4 + j] = 0;
			for (int k = 0; k < 4; k++) {
				out[i * 4 + j] += first[i * 4 + k] * second[j + k * 4];
			}
			if (print) std::cout << out[i * 4 + j] << " ";
		}
		if (print) std::cout << std::endl;
	}
	if (print) std::cout << std::endl;
}

/* Update the rotation of a given space based on its specified pitch, yaw, and roll. */
void updateRotation(Spaces* s, float dTime) {
	// This method is ugly, sorry. Converts Pitch/Yaw/Roll to Quaternion to a Rotation matrix.
	float pitch = s->quaternion[0] * 100 / 2.0f * M_PI / 180.0f;
	float yaw = s->quaternion[1] * 100 / 2.0f * M_PI / 180.0f;
	float roll = s->quaternion[2] * 100 / 2.0f * M_PI / 180.0f;
	float sinp = sinf(pitch), siny = sinf(yaw), sinr = sinf(roll), cosp = cosf(pitch), cosy = cosf(yaw), cosr = cosf(roll);
	float x = sinr * cosp * cosy - cosr * sinp * siny;
	float y = cosr * sinp * cosy + sinr * cosp * siny;
	float z = cosr * cosp * siny - sinr * sinp * cosy;
	float w = cosr * cosp * cosy + sinr * sinp * siny;
	double n = sqrt(x*x + y*y + z*z + w*w);
	x /= n;
	y /= n;
	z /= n;
	w /= n;
	s->rotation[0] = 1 - 2 * (y*y + z*z);
	s->rotation[1] = 2 * (x*y - w*z);
	s->rotation[2] = 2 * (x*z + w*y);
	s->rotation[4] = 2 * (x*y + w*z);
	s->rotation[5] = 1 - 2 * (x*x + z*z);
	s->rotation[6] = 2 * (y*z - w*x);
	s->rotation[8] = 2 * (x*z - w*y);
	s->rotation[9] = 2 * (y*z + w*x);
	s->rotation[10] = 1 - 2 * (x*x + y*y);
}

/* Reupdate a given Space's final transformation matrix based on its components. */
void updatePosition(Spaces* s) {
	GLfloat o[16];
	matrixMultiply(s->translation, s->rotation, o);
	matrixMultiply(o, s->scale, s->val);
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

/* Event activation for the window, borrowed in part from an online tutorial. */
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
		return;
	}
	for (int i = 0; i < 17; i++)
		if (keyNames[i] == key) {
			if (action != keys[i])
				keys[i] = action;
			return;
		}
}

/* This function performs all modifications to the model based on key input. */
void keyMagic(float dTime) {
	if (keys[0] != GLFW_RELEASE) {  // Move forward.
		viewSpace.translation[11] += dTime;
		if (viewSpace.translation[11] > 20)
			viewSpace.translation[11] = 20;
		updatePosition(&viewSpace);
	}
	if (keys[1] != GLFW_RELEASE) {  // Move backward.
		viewSpace.translation[11] -= dTime;
		if (viewSpace.translation[11] < -20)
			viewSpace.translation[11] = -20;
		updatePosition(&viewSpace);
	}
	if (keys[2] != GLFW_RELEASE) {  // Model up.
		modelSpace.translation[7] += dTime;
		updatePosition(&modelSpace);
	}
	if (keys[3] != GLFW_RELEASE) {  // Model down.
		modelSpace.translation[7] -= dTime;
		updatePosition(&modelSpace);
	}
	if (keys[4] != GLFW_RELEASE) {  // Model left.
		modelSpace.translation[3] -= dTime;
		updatePosition(&modelSpace);
	}
	if (keys[5] != GLFW_RELEASE) {  // Model right.
		modelSpace.translation[3] += dTime;
		updatePosition(&modelSpace);
	}
	if (keys[6] != GLFW_RELEASE) {  // Model forward.
		modelSpace.translation[11] += dTime;
		updatePosition(&modelSpace);
	}
	if (keys[7] != GLFW_RELEASE) {  // Model backward.
		modelSpace.translation[11] -= dTime;
		updatePosition(&modelSpace);
	}
	if (keys[8] != GLFW_RELEASE) {  // Model shrink.
		modelSpace.scale[0] -= dTime;
		modelSpace.scale[5] -= dTime;
		modelSpace.scale[10] -= dTime;
		updatePosition(&modelSpace);
	}
	if (keys[9] != GLFW_RELEASE) {  // Model grow.
		modelSpace.scale[0] += dTime;
		modelSpace.scale[5] += dTime;
		modelSpace.scale[10] += dTime;
		updatePosition(&modelSpace);
	}
	if (keys[10] != GLFW_RELEASE) {  // Model rotate x clockwise.
		modelSpace.quaternion[0] -= dTime;
		updateRotation(&modelSpace, dTime);
		updatePosition(&modelSpace);
	}
	if (keys[11] != GLFW_RELEASE) {  // Model rotate x counter.
		modelSpace.quaternion[0] += dTime;
		updateRotation(&modelSpace, dTime);
		updatePosition(&modelSpace);
	}
	if (keys[12] != GLFW_RELEASE) {  // Model rotate y clockwise.
		modelSpace.quaternion[1] -= dTime;
		updateRotation(&modelSpace, dTime);
		updatePosition(&modelSpace);
	}
	if (keys[13] != GLFW_RELEASE) {  // Model rotate y counter.
		modelSpace.quaternion[1] += dTime;
		updateRotation(&modelSpace, dTime);
		updatePosition(&modelSpace);
	}
	if (keys[14] != GLFW_RELEASE) {  // Model rotate z clockwise.
		modelSpace.quaternion[2] += dTime;
		updateRotation(&modelSpace, dTime);
		updatePosition(&modelSpace);
	}
	if (keys[15] != GLFW_RELEASE) {  // Model rotate z counter.
		modelSpace.quaternion[2] -= dTime;
		updateRotation(&modelSpace, dTime);
		updatePosition(&modelSpace);
	}
	if (keys[16] == GLFW_RELEASE) {  // Projection view change.
		if (projectionPressed) {
			projectionActive = !projectionActive;
			projectionPressed = false;
		}
	}
	if (keys[16] != GLFW_RELEASE) {  // Other half to projection view change.
		projectionPressed = true;
	}
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
	GLFWwindow* window;

	glfwSetErrorCallback(errorCallback);

	/* Initialize the library */
	if (!glfwInit()) { return -1; }

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	// tell glfw what to do on resize
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

	// init glad
	if (!gladLoadGL()) {
		std::cerr << "Failed to initialize OpenGL context" << std::endl;
		glfwTerminate();
		return -1;
	}

	/* init the model */
	float vertices[] = {
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		 0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		 0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

		 0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
		 0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		 0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
		 0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
		 0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
	};
	int sides = 8;
	float a = 360.0 / sides / 2;
	float x = tan(a) / 2;
	float cylinder[] = {
		-0.5f, x, -0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f, -x, -0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f, -x, -0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -0.5f, 1.0f, 0.0f, 0.0f,
		-x, -0.5, -0.5f, 1.0f, 0.0f, 0.0f,
		-x, -0.5, -0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -0.5f, 1.0f, 0.0f, 0.0f,
		x, -0.5, -0.5f, 1.0f, 0.0f, 0.0f,
		x, -0.5, -0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -0.5f, 1.0f, 0.0f, 0.0f,
		0.5f, -x, -0.5f, 1.0f, 0.0f, 0.0f,
		0.5f, -x, -0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -0.5f, 1.0f, 0.0f, 0.0f,
		0.5f, x, -0.5f, 1.0f, 0.0f, 0.0f,
		0.5f, x, -0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -0.5f, 1.0f, 0.0f, 0.0f,
		x, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
		x, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -0.5f, 1.0f, 0.0f, 0.0f,
		-x, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
		-x, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f, x, -0.5f, 1.0f, 0.0f, 0.0f,

		-0.5f, x, 0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f, -x, 0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f, -x, 0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 1.0f, 0.0f, 0.0f,
		-x, -0.5, 0.5f, 1.0f, 0.0f, 0.0f,
		-x, -0.5, 0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 1.0f, 0.0f, 0.0f,
		x, -0.5, 0.5f, 1.0f, 0.0f, 0.0f,
		x, -0.5, 0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 1.0f, 0.0f, 0.0f,
		0.5f, -x, 0.5f, 1.0f, 0.0f, 0.0f,
		0.5f, -x, 0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 1.0f, 0.0f, 0.0f,
		0.5f, x, 0.5f, 1.0f, 0.0f, 0.0f,
		0.5f, x, 0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 1.0f, 0.0f, 0.0f,
		x, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,
		x, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 1.0f, 0.0f, 0.0f,
		-x, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,
		-x, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f, x, 0.5f, 1.0f, 0.0f, 0.0f,

		-0.5f, x, 0.5f, 0.0f, 1.0f, 0.0f,
		-0.5f, x, -0.5f, 0.0f, 1.0f, 0.0f,
		-x, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
		-x, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
		-0.5f, x, -0.5f, 0.0f, 1.0f, 0.0f,
		-x, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
		-x, 0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
		-x, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
		x, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
		x, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
		-x, 0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
		x, 0.5f, -0.5f, 0.0f, 0.0f, 1.0f

		// ...etc, sorry this file is already ridiculously long,
		//	I think the above code demonstrates the basics of a
		//	cylinder well enough.
	};

	// copy vertex data
	GLuint VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// describe vertex layout
	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
		(void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
		(void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	// create the shaders
	Shader shader("../vert.glsl", "../frag.glsl");

	// setup the textures
	shader.use();

	// and use z-buffering
	glEnable(GL_DEPTH_TEST);
	glfwSetKeyCallback(window, key_callback);

	// Set up the various spaces.
	projectionSpace.name = "Projection";
	orthographSpace.name = "Orthograph";
	viewSpace.name = "View";
	modelSpace.name = "Model";
	for (int i = 0; i < 16; i++) {  // Default each space to the identity matrix.
		if (i % 5 == 0) {
			projectionSpace.translation[i] = 1;
			projectionSpace.rotation[i] = 1;
			projectionSpace.scale[i] = 1;
			orthographSpace.translation[i] = 1;
			orthographSpace.rotation[i] = 1;
			orthographSpace.scale[i] = 1;
			viewSpace.translation[i] = 1;
			viewSpace.rotation[i] = 1;
			viewSpace.scale[i] = 1;
			modelSpace.translation[i] = 1;
			modelSpace.rotation[i] = 1;
			modelSpace.scale[i] = 1;
		}
		else {
			projectionSpace.translation[i] = 0;
			projectionSpace.rotation[i] = 0;
			projectionSpace.scale[i] = 0;
			orthographSpace.translation[i] = 0;
			orthographSpace.rotation[i] = 0;
			orthographSpace.scale[i] = 0;
			viewSpace.translation[i] = 0;
			viewSpace.rotation[i] = 0;
			viewSpace.scale[i] = 0;
			modelSpace.translation[i] = 0;
			modelSpace.rotation[i] = 0;
			modelSpace.scale[i] = 0;
		}
	}

	// Initialize the Projection and Orthographic spaces.
	float ar = 1.0 * SCREEN_WIDTH / SCREEN_HEIGHT;
	float fov = M_PI * (60 / 2.0) / 180.0f;
	float zNear = 1, zFar = 100;
	double r = ar, l = -r, t = 1, b = -1;
	projectionSpace.translation[0] = 2 * zNear / (r - l);
	projectionSpace.translation[2] = (r + l) / (r - l);
	projectionSpace.translation[5] = 2 * zNear / (t - b);
	projectionSpace.translation[6] = (t + b) / (t - b);
	projectionSpace.translation[10] = -(zFar + zNear) / (zFar - zNear);
	projectionSpace.translation[11] = -2 * zFar * zNear / (zFar - zNear);
	projectionSpace.translation[14] = -1;
	projectionSpace.translation[15] = 0;
	orthographSpace.translation[0] = 2 / (r - l);
	orthographSpace.translation[3] = -(r + l) / (r - l);
	orthographSpace.translation[5] = 2 / (t - b);
	orthographSpace.translation[7] = -(t + b) / (t - b);
	orthographSpace.translation[10] = -2 / (zFar - zNear);
	orthographSpace.translation[11] = -(zFar + zNear) / (zFar - zNear);

	// Start the camera at z=-2.
	viewSpace.translation[11] = -2;

	// Update every space's matricies once before beginning to loop.
	updatePosition(&projectionSpace);
	updatePosition(&orthographSpace);
	updatePosition(&viewSpace);
	updatePosition(&modelSpace);

	// Saves the last updated time, for use in key input press duration.
	double lastTime = glfwGetTime();

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate shader
        shader.use();
		double curTime = glfwGetTime();
		float dTime = curTime - lastTime;
		lastTime = curTime;

		// Here we perform the key inputs for movement.
		keyMagic(dTime);

		GLuint sProjection = glGetUniformLocation(shader.id(), "projection");
		GLuint sView = glGetUniformLocation(shader.id(), "view");
		GLuint sModel = glGetUniformLocation(shader.id(), "model");
		if (sProjection == -1 || sView == -1 || sModel == -1)  // Error case.
			std::cout << "ERROR: shader value not found" << std::endl;

		if (projectionActive)  // Whether we're in Perspective or Orthographic mode.
			glUniformMatrix4fv(sProjection, 1, GL_TRUE, projectionSpace.val);
		else
			glUniformMatrix4fv(sProjection, 1, GL_TRUE, orthographSpace.val);
		glUniformMatrix4fv(sView, 1, GL_TRUE, viewSpace.val);
		glUniformMatrix4fv(sModel, 1, GL_TRUE, modelSpace.val);

        // render the cube
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
