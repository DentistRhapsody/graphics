# Program 3
*Christopher McCabe*

# Concept
The basic concept behind this program is to be able to place an arbitrary
series of triangles in 3-space and to project each of the elements to the
screen using **ray tracing**. All triangles will appear in the scene,
affected by standard Phong shading.

# How To Use
All elements are hard coded, but their generation is not. A custom struct
called "Triangle" can be declared freely, and will be rendered accurately
in the world so long as it is passed through to the rendering loop.

The vector array of Triangles declared in the main function, called
"allPolys", is a polygon soup of all triangles in the scene. For sample
viewing, it loads two cubes and a sphere (both are hard-coded data
structures as well). Adding more polygons is as simple as appending
the "allPolys" array.

# Other Notes
A number of mysterious bugs still exist in the code, but time constraints
will leave them as-is. Notably, certain spheres have odd shading issues,
potentially due to holes in their geometry.