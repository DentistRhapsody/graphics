#include <iostream>

#include <glm/glm.hpp>

#include "bitmap_image.hpp"

const int WIDTH = 640;
const int HEIGHT = 480;
const rgb_t BACKGROUND = make_colour(200, 200, 255);

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Ray {
	int id;
	glm::vec3 origin;
	glm::vec3 direction;

	Ray(const glm::vec3& origin = glm::vec3(0, 0, 0),
		const glm::vec3& direction = glm::normalize(glm::vec3(1, 1, 1)))
		: origin(origin), direction(glm::normalize(direction)) {
		static int id_seed = 0;
		id = ++id_seed;
	}
};

struct Shape {
	int id;
	glm::vec3 color;

	Shape(const glm::vec3& color=glm::vec3(0.1, 0.1, 0.1)) : color(color) {
		static int id_seed = 0;
		id = ++id_seed;
	}

	/** Function that returns the distance along the passed ray that this shape lies. **/
	virtual float tDistance(const Ray& r) = 0;
};

struct Sphere : Shape {
    glm::vec3 center;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), Shape(color) { }

	/* Implementation of sphere-intercept ray tracing. Returns -1 if there is no intersection. */
	float tDistance(const Ray& r) {  
		glm::vec3 toCenter = center - r.origin;
		float v = glm::dot(toCenter, r.direction);
		float disc = radius * radius - (glm::dot(toCenter, toCenter) - v * v);
		if (disc > 0) {
			float d = sqrt(disc);
			return v - d;
		}
		return -1;
	};
};

/** I added an extra parameter specifically for perspective (the 'd' value). If the cameraOffset
 *  parameter is negative, we will use orthographic projection. Otherwise, use the value for perspective.
**/
void render(bitmap_image& image, const std::vector<Sphere>& world, const float cameraOffset=-1) {
	Viewport vp(glm::vec2(-5, -5), glm::vec2(5, 5));
	float l = vp.min.x, r = vp.max.x, b = vp.min.y, t = vp.max.y;
	for (int x = 0; x < WIDTH; x++) {
		for (int y = 0; y < HEIGHT; y++) {
			// Build the ray for this specific pixel.
			float ux = l + (r - l) * (x + 0.5) / WIDTH;
			float vy = b + (t - b) * (y + 0.5) / HEIGHT;
			Ray r;
			if (cameraOffset < 0)  // Orthograpihc
				r = Ray(glm::vec3(ux, -vy, 0), glm::vec3(0, 0, 1));
			else  // Perspective
				r = Ray(glm::vec3(0, 0, -cameraOffset), glm::vec3(ux, -vy, cameraOffset));

			float mindis = INT_MAX;  // The shortest distance found along this ray.
			rgb_t pixel = BACKGROUND;  // The color of the object at the above shortest distance.
			for (Sphere sphere : world) {
				float distance = sphere.tDistance(r);
				if (distance < mindis && distance > 0) {  // If this sphere is closer than the others...
					mindis = distance;
					pixel = make_colour(sphere.color.x * 255, sphere.color.y * 255, sphere.color.z * 255);
				}
			}
			image.set_pixel(x, y, pixel);
		}
	}
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(WIDTH, HEIGHT);
	image.set_all_channels(0, 0, 0);
	// image_drawer g(image);

	// build world
	std::vector<Sphere> world = {
		Sphere(glm::vec3(0, 0, 1), 1, glm::vec3(1,1,0)),
		Sphere(glm::vec3(1, 1, 4), 2, glm::vec3(0,1,1)),
		Sphere(glm::vec3(2, 2, 6), 3, glm::vec3(1,0,1)),
		Sphere(glm::vec3(-2, -2, 20), 15, glm::vec3(0.2, 0.2, 0.2)),  // VERY big sphere in the background
		Sphere(glm::vec3(0.5, 0.5, 0.75), 0.5, glm::vec3(0.7, 0, 0.2)),  // tiny tumor sphere
    };

    // render the world
    render(image, world);
    image.save_image("../orthographic.bmp");
	render(image, world, 5);
	image.save_image("../perspective.bmp");
    std::cout << "Success" << std::endl;
}


