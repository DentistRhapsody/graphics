#define _USE_MATH_DEFINES
#include <iostream>
#include "matrix.h"
#include "vector.h"

Matrix::Matrix() {
	identify();
}

int Matrix::z(int col, int row) { return 4 * col + row % 4;  }

void Matrix::identify() {
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			vals[z(i, j)] = i==j;
}

float Matrix::determinant() {
	return (vals[0] * vals[4] * vals[8]) + (vals[1] * vals[5] * vals[6]) + (vals[2] * vals[3] * vals[7]) -
			(vals[2] * vals[4] * vals[6]) - (vals[0] * vals[5] * vals[7]) - (vals[1] * vals[3] * vals[8]);
}

void Matrix::rotateX(float theta) {
	identify();
	vals[z(1, 1)] = std::cos(theta);
	vals[z(1, 2)] = -std::sin(theta);
	vals[z(2, 1)] = std::sin(theta);
	vals[z(2, 2)] = std::cos(theta);
}

void Matrix::rotateY(float theta) {
	identify();
	vals[z(0, 0)] = std::cos(theta);
	vals[z(0, 2)] = -std::sin(theta);
	vals[z(2, 0)] = std::sin(theta);
	vals[z(2, 2)] = std::cos(theta);
}

void Matrix::rotateZ(float theta) {
	identify();
	vals[z(0, 0)] = std::cos(theta);
	vals[z(0, 1)] = -std::sin(theta);
	vals[z(1, 0)] = std::sin(theta);
	vals[z(1, 1)] = std::cos(theta);
}

void Matrix::scale(float xScale, float yScale, float zScale) {
	identify();
	vals[z(0, 0)] = xScale;
	vals[z(1, 1)] = yScale;
	vals[z(2, 2)] = zScale;
}

void Matrix::translate(float xShift, float yShift, float zShift) {
	identify();
	vals[z(0, 3)] = xShift;
	vals[z(1, 3)] = yShift;
	vals[z(2, 3)] = zShift;
}

Matrix Matrix::transpose() {
	Matrix out;
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			out.vals[z(i, j)] = vals[z(j, i)];
	/*for (int i = 0; i < 4; i++)
		out.vals[z(4, i)] = vals[z(4, i)];
	for (int i = 0; i < 4; i++)
		out.vals[z(i, 4)] = vals[z(i, 4)];*/
	return out;
}

Matrix Matrix::invert() {
	Matrix minors;
	minors.vals[z(0, 0)] = vals[z(1, 1)] * vals[z(2, 2)] - vals[z(1, 2)] * vals[z(2, 1)];
	minors.vals[z(0, 1)] = vals[z(1, 0)] * vals[z(2, 2)] - vals[z(1, 2)] * vals[z(2, 0)];
	minors.vals[z(0, 2)] = vals[z(1, 0)] * vals[z(2, 1)] - vals[z(1, 1)] * vals[z(2, 0)];
	minors.vals[z(1, 0)] = vals[z(0, 1)] * vals[z(2, 2)] - vals[z(0, 2)] * vals[z(2, 1)];
	minors.vals[z(1, 1)] = vals[z(0, 0)] * vals[z(2, 2)] - vals[z(0, 2)] * vals[z(2, 0)];
	minors.vals[z(1, 2)] = vals[z(0, 0)] * vals[z(2, 1)] - vals[z(0, 1)] * vals[z(2, 0)];
	minors.vals[z(2, 0)] = vals[z(0, 1)] * vals[z(1, 2)] - vals[z(0, 2)] * vals[z(1, 1)];
	minors.vals[z(2, 1)] = vals[z(0, 0)] * vals[z(1, 2)] - vals[z(0, 2)] * vals[z(1, 0)];
	minors.vals[z(2, 2)] = vals[z(0, 0)] * vals[z(1, 1)] - vals[z(0, 1)] * vals[z(1, 0)];
	
	minors.vals[z(1, 0)] *= -1;
	minors.vals[z(0, 1)] *= -1;
	minors.vals[z(2, 1)] *= -1;
	minors.vals[z(1, 2)] *= -1;

	Matrix adjoint = minors.transpose();
	float det = adjoint.determinant();
	return adjoint * det;
}

bool Matrix::isInvertible() {
	return determinant() != 0;
}

Matrix Matrix::operator*(float scalar) {
	Matrix x;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			x.vals[z(i, j)] = vals[z(i, j)] * scalar;
		}
	}
	return x;
}

Matrix Matrix::operator*(Matrix& m) {
	Matrix out;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			out.vals[z(i, j)] = 0;
			for (int r = 0; r < 4; r++)
				out.vals[z(i, j)] += m.vals[z(r, j)] * vals[z(i, r)];
		}
	}
	return out;
}

// Borrowed functions

void Matrix::look_at(Vector& eye, Vector& target, Vector& up) {
	Vector gaze = target - eye;
	Vector w;
	gaze.normalized(&w);
	w.scale(-1);
	Vector u = up.cross(w);
	up.normalized(&u);
	Vector v = w.cross(u);

	Matrix camera;
	Vector* basis[3] = { &u, &v, &w };
	for (int i = 0; i < 3; ++i) {
			camera.vals[camera.z(i, 0)] = basis[i]->x;
			camera.vals[camera.z(i, 1)] = basis[i]->y;
			camera.vals[camera.z(i, 2)] = basis[i]->z;
	}

	Matrix translate;
	translate.translate(-eye.x, -eye.y, -eye.z);

	identify();
	Matrix x = camera * translate;
	for (int i = 0; i < 16; i++) {
		vals[i] = x.vals[i];
	}
}

void Matrix::ortho(float l, float r, float b, float t, float n, float f) {
	identify();
	vals[z(0, 0)] = 2 / (r - l);
	vals[z(0, 3)] = -(r + l) / (r - l);

	vals[z(1, 1)] = 2 / (t - b);
	vals[z(1, 3)] = -(t + b) / (t - b);

	vals[z(2, 2)] = -2 / (f - n);
	vals[z(2, 3)] = -(f + n) / (f - n);
}

void Matrix::perspective(float min_fov, float aspect_ratio, float n, float f) {
	identify();

	double theta = min_fov*M_PI / 180.0;

	vals[z(0, 0)] = 1 / (aspect_ratio*tan(theta));
	vals[z(1, 1)] = 1 / (tan(theta));
	vals[z(2, 2)] = -(f + n) / (f - n);
	vals[z(2, 3)] = (-2 * f*n) / (f - n);
	vals[z(3, 2)] = -1;
	vals[z(3, 3)] = 0;
}
