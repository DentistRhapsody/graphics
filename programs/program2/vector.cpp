#include <iostream>
#include "vector.h"

Vector::Vector() : x(0.0f), y(0.0f), z(0.0f), w(1.0f) { }
//Vector::Vector(float xCoord, float yCoord, float zCoord) : x(xCoord), y(yCoord), z(zCoord), w(1.0f) { }
Vector::Vector(float xCoord, float yCoord, float zCoord, float wVal) : x(xCoord), y(yCoord), z(zCoord), w(wVal) { }

void Vector::normalize() { normalized(this); }
void Vector::normalized(Vector* out) {
	float norm = sqrt(x * x + y * y + z * z);
	out->x = x / norm;
	out->y = y / norm;
	out->z = z / norm;
	out->w = w;
}

float Vector::determinant(float a, float b, float c, float d) {
	return a*d - c*b;
}

void Vector::scale(float scalar) {
	x *= scalar;
	y *= scalar;
	z *= scalar;
}

Vector& Vector::cross(Vector& v) {
	return Vector(determinant(y, z, v.y, v.z), -determinant(x, z, v.x, v.z), determinant(x, y, v.x, v.y), w);
}

Vector& Vector::operator+(Vector& v) {
	return Vector(x + v.x, y + v.y, z + v.z, w);
}

Vector& Vector::operator-(Vector& v) {
	return Vector(x - v.x, y - v.y, z - v.z, w);
}

bool Vector::operator==(Vector& v) {
	return (x == v.x && y == v.y && z == v.z && w == v.w);
}

std::ostream& Vector::operator<<(std::ostream& os) {
	return os << "(" << x << ", " << y << ", " << z << ", " << w << ")";
}