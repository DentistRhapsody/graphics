
class LinkedList {
private:
	struct node {
		int data;
		node *next;
	};
	node *head, *tail;


public:
	LinkedList() {
		head = NULL;
		tail = NULL;
	}

	void createNode(int value) {
		node *temp = new node;
		temp->data = value;
		temp->next = NULL;
		if (head==NULL) {
			head = temp;
			tail = temp;
			temp = NULL;
		}
		else {
			tail->next = temp;
			tail = temp;
		}
	}

	void del(int data)
	{
		node *current = new node;
		node *previous = new node;
		current = head;
		int v = current->data;
		while (v != data && current->next != NULL)
		{
			previous = current;
			current = current->next;
			v = current->data;
		}
		if (v == current->data)
			previous->next = current->next;
		else
			std::cout << "Error with list deletion: could not find specified value!" << std::endl;
	}

};