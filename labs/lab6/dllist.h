#include <cstdlib>
#include <vector>

class Edge {
	std::pair<Vector*, Vector*> edge;

public:

	Edge() {
		Vector v1 = Vector(0, 0, 0, 1);
		Vector v2 = Vector(1, 1, 1, 1);
		edge = std::make_pair(v1, v1);
	}

	Edge(Vector* origin, Vector* end) {
		edge = std::make_pair(origin, end);
	}

	Vector* origin() { return edge.first; }
	Vector* end() { return edge.second; }
	void setOrigin(Vector* v) { edge.first = v; }
	void setEnd(Vector* v) { edge.second = v; }
	
	Edge twin() {
		return Edge(end(), origin());
	}

	bool operator==(Edge& e) {
		if (e.origin() == origin() && e.end() == end())
			return true;
		return false;
	}

	bool operator!=(Edge& e) {
		if (operator==(e))
			return false;
		return true;
	}
};

class Face {
	Element* boundary = NULL;
	
	Face(Element* e) {
		boundary = e;
	}
};

class Element {
	Element* lastElement = NULL;
	Element* nextElement = NULL;
	Vector* vertex = NULL;
	Edge* edge = NULL;
	Face* face = NULL;

public:
	Element(Vector* v) {
		vertex = v;
	}

	void tail(Vector* v) { tail(new Element(v)); }
	void tail(Element* v) {
		if (nextElement == NULL) {
			nextElement = v;
			nextElement->lastElement = this;
		}
		else
			nextElement->tail(v);
	}

	void append(Vector* v) { append(new Element(v)); }
	void append(Element* e) {
		e->lastElement = this;
		e->nextElement = nextElement;
		nextElement = e;
	}

	void prepend(Vector* v) { prepend(new Element(v)); }
	void prepend(Element* e) {
		e->nextElement = this;
		e->lastElement = lastElement;
		lastElement = e;
	}

	void del() {
		nextElement->lastElement = lastElement;
		lastElement->nextElement = nextElement;
		delete this;
	}

	void operator+(Element& e) { tail(&e); }
	void operator-(Element& e) {
		Element* le = e.lastElement;
		Element* ne = e.nextElement;
		if (le != NULL)
			le->nextElement = ne;
		if (ne != NULL)
			ne->lastElement = le;
		e.nextElement = NULL;
		e.lastElement = NULL;
	}
	/*bool operator==(Element& e) {
		if (lastElement == e.lastElement && nextElement == e.nextElement && vertex == e.vertex && face == e.face && edge == e.edge)
			return true;
		return false;
	}*/

	Element* next() { return nextElement; }
	Element* previous() { return lastElement; }
	Vector* vertex() { return vertex; }
	Edge* edge() { return edge; }

	Element* twin() {
		Edge t = edge->twin();
		Element* iter = first();
		while (iter->nextElement != NULL) {
			if (iter->edge == &t)
				return iter;
			iter = iter->next();
		}
		return NULL;
	}

	Element* first() {
		Element* curE = this;
		while (curE->lastElement != NULL) {
			curE = curE->previous();
		}
		return curE;
	}

	Element* last() {
		Element* curE = this;
		while (curE->nextElement != NULL) {
			curE = curE->next();
		}
		return curE;
	}
};
